"""remove refund_unique_id and add payment_unique_id colum

Revision ID: 369183263d9c
Revises: b99a3b2d6425
Create Date: 2021-10-18 15:31:59.119991

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "369183263d9c"
down_revision = "b99a3b2d6425"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("payments", sa.Column("payment_unique_id", sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("payments", "payment_unique_id")
    # ### end Alembic commands ###
