# Sending webhooks

## create_webhook

Create webhook is called from the endpoints

```mermaid
stateDiagram-v2
state "webhook_data: dict,<br>notification_url: str,<br>payment: Payment" as create_webhook

[*] --> create_webhook
create_webhook --> create_webhook_in_database
create_webhook_in_database --> send_webhook
send_webhook --> [*]: via celery
```

## send_webhook

Send webhook is called from create_webhook and from retry_webhooks

```mermaid
stateDiagram-v2
state "webhook_id: UUID" as send_webhook
state is_webhook <<choice>>
state response <<choice>>
state attempts <<choice>>

[*] --> send_webhook
send_webhook --> get_webhook_by_id
get_webhook_by_id --> is_webhook: is webhook None?
is_webhook --> [*]: yes
is_webhook --> send_webhook_request: no
send_webhook_request --> response: delivered successfully?
response --> update_webhook(delivered): yes
response --> attempts: have we tried enough?
attempts --> update_webhook(failed): yes
attempts --> send_webhook_later: no
update_webhook(failed) --> [*]
update_webhook(delivered) --> [*]
send_webhook_later --> [*]
```

## Proposed DB Structure

```mermaid
classDiagram
direction RL

class Webhook {
    id : UUID
    payment_id : UUID
    unique_id : str
    created_at : Optional[datetime]
    result_code : str
    result_description : Optional[str]
    has_delivered : bool
    data : str
    success : Optional[bool]
    notification_url : AnyHttpUrl
    headers : str
    attempt : Optional[int]
    timestamp : Optional[datetime]
    trigger_timestamp : int
}
```
