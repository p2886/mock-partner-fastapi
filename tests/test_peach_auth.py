import mock
from fastapi.testclient import TestClient
from peachpayments_partner.fixtures import UNIQUE_ID
from peachpayments_partner_pydantic.schemas import DebitResponse


@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.router.actions")
@mock.patch("app.utils.auth.settings")
def test_authentication_is_called(
    mock_settings, mock_peach_actions, mock_auth, client: TestClient, debit_request, debit_response, refund_request
):
    mock_settings.AUTH0_AUDIENCE = "someaudience"
    mock_peach_actions.status.return_value = DebitResponse(**debit_response)
    client.get("/v1/status/abc", headers={"Authorization": "token-status"})
    mock_auth.assert_called_once_with("token-status", audience="someaudience")
    mock_auth.reset_mock()

    mock_peach_actions.debit.return_value = DebitResponse(**debit_response)
    client.post("/v1/debit", json=debit_request, headers={"Authorization": "token-debit"})
    mock_auth.assert_called_once_with("token-debit", audience="someaudience")
    mock_auth.reset_mock()

    mock_peach_actions.refund.return_value = DebitResponse(**debit_response)
    client.post(f"/v1/refund/{UNIQUE_ID}", json=refund_request, headers={"Authorization": "token-refund"})
    mock_auth.assert_called_once_with("token-refund", audience="someaudience")
    mock_auth.reset_mock()
