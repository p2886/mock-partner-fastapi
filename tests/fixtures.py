from peachpayments_partner.fixtures import (
    AMOUNT,
    CURRENCY,
    MERCHANT_TRANSACTION_ID,
    NOTIFICATION_URL,
    PAYMENT_BRAND,
    PAYMENT_TYPE_DEBIT,
    SHOPPER_RESULT_URL,
    UNIQUE_ID,
)

PAYMENT_DATA_SUCCESS = {
    "unique_id": UNIQUE_ID,
    "amount": AMOUNT,
    "currency": CURRENCY,
    "payment_brand": PAYMENT_BRAND,
    "payment_type": PAYMENT_TYPE_DEBIT,
    "merchant_name": "Shopping Merchant",
    "merchant_transaction_id": MERCHANT_TRANSACTION_ID,
    "merchant_invoice_id": "20170630-4072-00",
    "notification_url": NOTIFICATION_URL,
    "shopper_result_url": SHOPPER_RESULT_URL,
    "custom_parameters": "{}",
    "status_code": "000.000.100",
}


PAYMENT_DATA_PENDING = {
    "unique_id": UNIQUE_ID,
    "amount": AMOUNT,
    "currency": CURRENCY,
    "payment_brand": PAYMENT_BRAND,
    "payment_type": PAYMENT_TYPE_DEBIT,
    "merchant_name": "Shopping Merchant",
    "merchant_transaction_id": MERCHANT_TRANSACTION_ID,
    "merchant_invoice_id": "20170630-4072-00",
    "notification_url": NOTIFICATION_URL,
    "shopper_result_url": SHOPPER_RESULT_URL,
    "custom_parameters": "{}",
    "status_code": "000.200.000",
}
