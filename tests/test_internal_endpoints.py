from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app.internal import actions, schemas
from app.models import Payment, Status


def test_payments_get(app: FastAPI, db_session: Session, client: TestClient, payment_data_pending):
    obj_in = schemas.PaymentCreate(**payment_data_pending)
    payment = actions.payment.create(db_session, obj_in=obj_in)

    response = client.get(f"/payments/{payment.id}")
    assert response.status_code == 200
    data = response.json()
    assert data["amount"] == payment.amount


def test_payment_get_404(app: FastAPI, db_session: Session, client: TestClient):
    response = client.get("/payments/a1202376c8d146728dac871d6f68b221")
    assert response.status_code == 404
    data = response.json()
    assert data == {"detail": "Payment not found"}


def test_payments_list(app: FastAPI, db_session: Session, client: TestClient, payment_data_pending):
    unique_id = "a1202376c8d146728dac871d6f68b22"
    for i in range(0, 10):
        data = payment_data_pending.copy()
        data["unique_id"] = f"{unique_id}{i}"
        obj_in = schemas.PaymentCreate(**data)
        actions.payment.create(db_session, obj_in=obj_in)

    response = client.get("/payments")
    payments: list[dict] = response.json()
    assert response.status_code == 200
    assert len(payments) == 10
    for payment in payments:
        assert payment["unique_id"].startswith(unique_id)


def test_statuses_list(app: FastAPI, db_session: Session, client: TestClient, payment_data_pending):
    payment: Payment = actions.payment.create(db_session, obj_in=schemas.PaymentCreate(**payment_data_pending))
    for i in range(100, 200):
        data = dict(
            status_code=f"100.100.{i}",
            status_description=f"Some fake code description{i}",
            payment_id=payment.id,
        )
        actions.status.create(db_session, obj_in=schemas.StatusCreate(**data))

    response = client.get("/statuses")
    statuses: list[dict] = response.json()
    assert response.status_code == 200
    assert len(statuses) == 100


def test_statuses_get(app: FastAPI, db_session: Session, client: TestClient, payment_data_pending):
    payment: Payment = actions.payment.create(db_session, obj_in=schemas.PaymentCreate(**payment_data_pending))
    obj_in = schemas.StatusCreate(**payment_data_pending, payment_id=payment.id)
    status: Status = actions.status.create(db_session, obj_in=obj_in)
    response = client.get(f"/statuses/{status.id}")
    data = response.json()
    assert response.status_code == 200
    assert data["status_code"] == status.status_code


def test_status_get_404(app: FastAPI, db_session: Session, client: TestClient):
    response = client.get("/statuses/a1202376c8d146728dac871d6f68b221")
    data = response.json()
    assert response.status_code == 404
    assert data == {"detail": "Status not found"}


def test_get_statuses_by_payment_id(app: FastAPI, db_session: Session, client: TestClient, payment_data_pending):
    payment: Payment = actions.payment.create(db_session, obj_in=schemas.PaymentCreate(**payment_data_pending))
    for i in range(100, 200):
        data = dict(
            status_code=f"100.100.{i}",
            status_description=f"Some fake code description{i}",
            payment_id=payment.id,
        )
        actions.status.create(db_session, obj_in=schemas.StatusCreate(**data))

    response = client.get(f"/payments/{payment.id}/statuses")
    statuses: list[dict] = response.json()
    assert response.status_code == 200
    assert len(statuses) == 100
