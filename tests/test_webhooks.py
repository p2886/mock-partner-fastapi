import json
import logging
from datetime import datetime

import mock
import pytest
import requests
from freezegun import freeze_time
from peachpayments_partner.fixtures import TIMESTAMP, UNIQUE_ID

from app.config import settings
from app.internal.actions import payment
from app.internal.schemas import Payment
from app.webhooks.actions import create_webhook, get_trigger_timestamp, send_webhook, set_backoff


@freeze_time(TIMESTAMP)
@pytest.mark.parametrize(
    "attempt,delta",
    (
        (1, 60),
        (2, 120),
        (3, 240),
        (4, 480),
        (5, 900),
        (6, 1800),
        (7, 3600),
        (8, 86400),
        (9, 86400),
        (20, 86400),
    ),
)
def test_get_trigger_timestamp(attempt, delta):
    timestamp = 1619163685
    assert get_trigger_timestamp(attempt) == timestamp + delta


@freeze_time(TIMESTAMP)
@mock.patch("app.webhooks.actions.send_webhook")
def test_create_webhook_success(mock_send_webhook, db_session, webhook_request, payment_data_pending):
    transaction = payment.create(db=db_session, obj_in=Payment(**payment_data_pending))

    webhook = create_webhook(db_session, webhook_request, transaction, "https://example.com")

    assert webhook.id is not None
    assert webhook.payment_id == transaction.id
    assert webhook.notification_url == "https://example.com"
    assert json.loads(webhook.data) == webhook_request
    assert webhook.trigger_timestamp == int(datetime.utcnow().timestamp())
    mock_send_webhook.delay.assert_called_once_with(webhook.id)


@mock.patch("app.webhooks.actions.send_webhook")
def test_create_webhook_wrong_payment(mock_send_webhook, db_session, webhook_request, payment_data_pending):
    transaction = payment.create(db=db_session, obj_in=Payment(**payment_data_pending))
    transaction.unique_id = transaction.id

    with pytest.raises(ValueError) as excinfo:
        create_webhook(db_session, webhook_request, transaction, "https://example.com")

    assert "Payment unique id does not match webhook unique id" in str(excinfo.value)
    mock_send_webhook.delay.assert_not_called()


@mock.patch("app.webhooks.actions.internal_actions.webhook.update")
@mock.patch("app.webhooks.actions.get_trigger_timestamp")
@mock.patch("app.webhooks.actions.send_webhook")
def test_set_backoff_calls(mock_send_webhook, mock_get_trigger_timestamp, mock_webhook_update):
    mock_get_trigger_timestamp.return_value = 10240
    webhook = mock.Mock(id="abc", trigger_timestamp=10000, attempt=1)
    db = mock.Mock()

    set_backoff(db, webhook)

    mock_get_trigger_timestamp.assert_called_once_with(2)
    mock_webhook_update.assert_called_once_with(db, db_obj=webhook, obj_in={"attempt": 2, "trigger_timestamp": 10240})
    mock_send_webhook.apply_async.assert_called_once_with(args=(webhook.id,), countdown=120)


@mock.patch("app.webhooks.actions.internal_actions.webhook.update")
@mock.patch("app.webhooks.actions.get_trigger_timestamp")
@mock.patch("app.webhooks.actions.send_webhook")
@mock.patch.object(logging.getLogger("app.webhooks.actions"), "warning")
def test_set_backoff_max_attempts(
    mock_logger_warning, mock_send_webhook, mock_get_trigger_timestamp, mock_webhook_update
):
    mock_get_trigger_timestamp.return_value = 10240
    webhook = mock.Mock(id="abc", unique_id="xxx", trigger_timestamp=10000, attempt=settings.MAX_WEBHOOK_ATTEMPT + 1)
    db = mock.Mock()

    set_backoff(db, webhook)

    mock_logger_warning.assert_called_once_with("uid=xxx - Max webhook attempts reached")
    mock_webhook_update.assert_called_once_with(db, db_obj=webhook, obj_in={"is_failed": True})
    mock_get_trigger_timestamp.assert_not_called()
    mock_send_webhook.apply_async.assert_not_called()


@mock.patch("app.webhooks.actions.internal_actions.webhook.get_active")
@mock.patch("app.webhooks.actions.internal_actions.webhook.update")
@mock.patch("app.webhooks.actions.get_access_token")
@mock.patch("app.webhooks.actions.TokenFormatter.format")
@mock.patch("app.webhooks.actions.requests.post")
@mock.patch("app.webhooks.actions.set_backoff")
@mock.patch("app.webhooks.actions.SessionLocal")
def test_send_webhook_success(
    mock_SessionLocal,
    mock_set_backoff,
    mock_requests_post,
    mock_tokenformatter_format,
    mock_get_access_token,
    mock_webhook_update,
    mock_webhook_get_active,
):
    db = mock.Mock()
    mock_SessionLocal.return_value = db
    webhook_data = {"foo": "bar"}
    webhook = mock.Mock(notification_url="https://example.com", unique_id="abc", data=json.dumps(webhook_data))
    mock_webhook_get_active.return_value = webhook
    data = {"result": {"code": "000.000.100"}, "timestamp": TIMESTAMP}
    mock_tokenformatter_format.return_value = "Bearer 12345"
    response = mock.Mock(status_code=200, text="", data=data, json=lambda: data)
    mock_requests_post.return_value = response

    send_webhook(UNIQUE_ID)

    mock_SessionLocal.assert_called_once()
    mock_webhook_get_active.assert_called_once_with(db, UNIQUE_ID)
    mock_get_access_token.assert_called_once()
    mock_requests_post.assert_called_once_with(
        webhook.notification_url,
        json=webhook_data,
        headers={"Authorization": "Bearer 12345", "Content-Type": "application/json"},
        timeout=5,
    )
    mock_webhook_update.assert_called_once_with(db, db_obj=webhook, obj_in={"is_delivered": True})
    mock_set_backoff.assert_not_called()
    db.close.assert_called_once()


@mock.patch("app.webhooks.actions.internal_actions.webhook.get_active")
@mock.patch("app.webhooks.actions.get_access_token")
@mock.patch("app.webhooks.actions.TokenFormatter.format")
@mock.patch("app.webhooks.actions.requests.post")
@mock.patch("app.webhooks.actions.set_backoff")
@mock.patch("app.webhooks.actions.SessionLocal")
def test_send_webhook_fail_no_webhook(
    mock_SessionLocal,
    mock_set_backoff,
    mock_requests_post,
    mock_tokenformatter_format,
    mock_get_access_token,
    mock_webhook_get_active,
):
    mock_webhook_get_active.return_value = None
    db = mock.Mock()
    mock_SessionLocal.return_value = db

    send_webhook(UNIQUE_ID)

    mock_webhook_get_active.assert_called_once_with(db, UNIQUE_ID)
    db.close.assert_called_once()
    mock_get_access_token.assert_not_called()
    mock_requests_post.assert_not_called()
    mock_set_backoff.assert_not_called()


@mock.patch("app.webhooks.actions.internal_actions.webhook.get_active")
@mock.patch("app.webhooks.actions.get_access_token")
@mock.patch("app.webhooks.actions.TokenFormatter.format")
@mock.patch("app.webhooks.actions.requests.post")
@mock.patch("app.webhooks.actions.set_backoff")
@mock.patch("app.webhooks.actions.SessionLocal")
def test_send_webhook_fail_connection_error(
    mock_SessionLocal,
    mock_set_backoff,
    mock_requests_post,
    mock_tokenformatter_format,
    mock_get_access_token,
    mock_webhook_get_active,
):
    webhook_data = {"foo": "bar"}
    webhook = mock.Mock(notification_url="https://example.com", unique_id="abc", data=json.dumps(webhook_data))
    mock_webhook_get_active.return_value = webhook
    mock_tokenformatter_format.return_value = "Bearer 12345"
    mock_requests_post.side_effect = requests.ConnectionError
    db = mock.Mock()
    mock_SessionLocal.return_value = db

    send_webhook(UNIQUE_ID)

    mock_webhook_get_active.assert_called_once_with(db, UNIQUE_ID)
    mock_get_access_token.assert_called_once()
    mock_requests_post.assert_called_once_with(
        webhook.notification_url,
        json=webhook_data,
        headers={"Authorization": "Bearer 12345", "Content-Type": "application/json"},
        timeout=5,
    )
    mock_set_backoff.assert_called_once_with(db, webhook)
    db.close.assert_not_called()


@pytest.mark.parametrize("status_code", [199, 300, 400, 401, 403, 404, 500])
@mock.patch("app.webhooks.actions.internal_actions.webhook.get_active")
@mock.patch("app.webhooks.actions.get_access_token")
@mock.patch("app.webhooks.actions.TokenFormatter.format")
@mock.patch("app.webhooks.actions.requests.post")
@mock.patch("app.webhooks.actions.set_backoff")
@mock.patch("app.webhooks.actions.SessionLocal")
def test_send_webhook_fail_response_not_OK_below_200(
    mock_SessionLocal,
    mock_set_backoff,
    mock_requests_post,
    mock_tokenformatter_format,
    mock_get_access_token,
    mock_webhook_get,
    status_code,
):
    webhook_data = {"foo": "bar"}
    webhook = mock.Mock(notification_url="https://example.com", unique_id="abc", data=json.dumps(webhook_data))
    mock_webhook_get.return_value = webhook
    data = {"result": {"code": "000.000.100"}, "timestamp": TIMESTAMP}
    mock_tokenformatter_format.return_value = "Bearer 12345"
    response = mock.Mock(status_code=status_code, text="", data=data, json=lambda: data)
    mock_requests_post.return_value = response
    db = mock.Mock()
    mock_SessionLocal.return_value = db

    send_webhook(UNIQUE_ID)

    mock_webhook_get.assert_called_once_with(db, UNIQUE_ID)
    mock_get_access_token.assert_called_once()
    mock_requests_post.assert_called_once_with(
        webhook.notification_url,
        json=webhook_data,
        headers={"Authorization": "Bearer 12345", "Content-Type": "application/json"},
        timeout=5,
    )
    mock_set_backoff.assert_called_once_with(db, webhook)
    db.close.assert_not_called()


@mock.patch("app.webhooks.actions.internal_actions.webhook.get_active")
@mock.patch("app.webhooks.actions.get_access_token")
@mock.patch("app.webhooks.actions.TokenFormatter.format")
@mock.patch("app.webhooks.actions.requests.post")
@mock.patch("app.webhooks.actions.set_backoff")
@mock.patch("app.webhooks.actions.SessionLocal")
@mock.patch.object(logging.getLogger("app.webhooks.actions"), "error")
def test_send_webhook_fail_response_not_json(
    mock_logger_error,
    mock_SessionLocal,
    mock_set_backoff,
    mock_requests_post,
    mock_tokenformatter_format,
    mock_get_access_token,
    mock_webhook_get_active,
):
    webhook_data = {"foo": "bar"}
    webhook = mock.Mock(notification_url="https://example.com", unique_id="abc", data=json.dumps(webhook_data))
    mock_webhook_get_active.return_value = webhook
    data = {"result": {"code": "000.000.100"}, "timestamp": TIMESTAMP}
    mock_tokenformatter_format.return_value = "Bearer 12345"
    response = mock.Mock(status_code=200, text="response text", data=data)
    response.json.side_effect = json.JSONDecodeError(msg="JSON Error", doc="JSON Error doc", pos=0)
    mock_requests_post.return_value = response
    db = mock.Mock()
    mock_SessionLocal.return_value = db

    send_webhook(UNIQUE_ID)

    mock_webhook_get_active.assert_called_once_with(db, UNIQUE_ID)
    mock_get_access_token.assert_called_once()
    mock_requests_post.assert_called_once_with(
        webhook.notification_url,
        json=webhook_data,
        headers={"Authorization": "Bearer 12345", "Content-Type": "application/json"},
        timeout=5,
    )
    mock_logger_error.assert_called_once_with(
        "uid=abc - Webhook response JSON decode error, " "response: 'response text'"
    )
    mock_set_backoff.assert_called_once_with(db, webhook)
    db.close.assert_not_called()


@mock.patch("app.webhooks.actions.internal_actions.webhook.get_active")
@mock.patch("app.webhooks.actions.get_access_token")
@mock.patch("app.webhooks.actions.TokenFormatter.format")
@mock.patch("app.webhooks.actions.requests.post")
@mock.patch("app.webhooks.actions.set_backoff")
@mock.patch("app.webhooks.actions.SessionLocal")
@mock.patch.object(logging.getLogger("app.webhooks.actions"), "error")
def test_send_webhook_fail_response_not_valid(
    mock_logger_error,
    mock_SessionLocal,
    mock_set_backoff,
    mock_requests_post,
    mock_tokenformatter_format,
    mock_get_access_token,
    mock_webhook_get,
):
    webhook_data = {"foo": "bar"}
    webhook = mock.Mock(notification_url="https://example.com", unique_id="abc", data=json.dumps(webhook_data))
    mock_webhook_get.return_value = webhook
    data = {"result": {"code": "000.000.100"}, "timestamp": TIMESTAMP}
    mock_tokenformatter_format.return_value = "Bearer 12345"
    response = mock.Mock(status_code=200, text="response text", data=data, json=lambda: {})
    mock_requests_post.return_value = response
    db = mock.Mock()
    mock_SessionLocal.return_value = db

    send_webhook(UNIQUE_ID)

    mock_webhook_get.assert_called_once_with(db, UNIQUE_ID)
    mock_get_access_token.assert_called_once()
    mock_requests_post.assert_called_once_with(
        webhook.notification_url,
        json=webhook_data,
        headers={"Authorization": "Bearer 12345", "Content-Type": "application/json"},
        timeout=5,
    )
    mock_set_backoff.assert_called_once_with(db, webhook)
    mock_logger_error.assert_called_once_with(
        "uid=abc - Webhook response validation error: ["
        "{'loc': ('result',), 'msg': 'field required', 'type': 'value_error.missing'}, "
        "{'loc': ('timestamp',), 'msg': 'field required', 'type': 'value_error.missing'}]"
    )
    db.close.assert_not_called()
