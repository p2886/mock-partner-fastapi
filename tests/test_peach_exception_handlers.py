import json

import mock
import pytest
from fastapi import Request
from peachpayments_partner.result_codes import result_codes

from app.peach.exception_handlers import validation_exception_handler


@pytest.fixture
def mock_request():
    endpoint = mock.Mock()
    endpoint.__name__ = "debit_request"
    return Request(scope=dict(type="http", endpoint=endpoint))


@pytest.mark.asyncio
@mock.patch("app.peach.exception_handlers.exception_to_response")
async def test_validation_exception_handler(m_exception_to_response, mock_request, timestamp):
    exc = mock.Mock()
    exc.errors.return_value = [dict(loc=("mocked"), msg="some error")]
    result_code = result_codes.INVALID_OR_MISSING_PARAMETER

    m_exception_to_response.return_value = dict(
        result={
            "code": result_code.code,
            "parameterErrors": [dict(name="path/to/the/value", value="value", message="some message")],
        },
        resultDetails=None,
        timestamp="2021-09-02T11:30:00.000000Z",
    )

    response = await validation_exception_handler(mock_request, exc)
    res_code = result_codes.INVALID_OR_MISSING_PARAMETER
    assert response.status_code == 400
    assert json.loads(response.body) == dict(
        result=dict(
            code=res_code.code,
            parameterErrors=[dict(message="some message", name="path/to/the/value", value="value")],
        ),
        resultDetails=None,
        timestamp="2021-09-02T11:30:00.000000Z",
    )


@pytest.mark.asyncio
@mock.patch("app.peach.exception_handlers.request_validation_exception_handler")
async def test_validation_exception_handler_not_peach(m_handler):
    endpoint = mock.Mock()
    endpoint.__name__ = "something_else"
    request = Request(scope=dict(type="http", endpoint=endpoint))

    exc = mock.Mock()
    exc.body = dict(container=dict(field="value"))
    exc.errors.return_value = [dict(loc=("body", "container", "field"), msg="some error")]

    await validation_exception_handler(request, exc)
    m_handler.assert_called_once_with(request, exc)


@pytest.mark.asyncio
@mock.patch("app.peach.exception_handlers.request_validation_exception_handler")
async def test_validation_exception_handler_not_body(m_handler, mock_request):

    exc = mock.Mock()
    exc.body = None

    await validation_exception_handler(mock_request, exc)
    m_handler.assert_called_once_with(mock_request, exc)
