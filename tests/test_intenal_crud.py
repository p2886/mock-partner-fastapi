from copy import copy

import pytest
from pydantic.error_wrappers import ValidationError

from app.internal import actions, schemas
from app.models import Payment, Status, Webhook


def test_payment_create(db_session, payment_data_pending):
    obj_in = schemas.PaymentCreate(**payment_data_pending)
    payment: Payment = actions.payment.create(db_session, obj_in=obj_in)
    assert payment.unique_id == payment_data_pending["unique_id"]


def test_payment_create_debit_payment_missing_shoppers_result_url(payment_data_pending):
    payment_data_pending["payment_type"] = "DB"
    payment_data_pending.pop("shopper_result_url")
    with pytest.raises(ValidationError) as exc_info:
        schemas.PaymentCreate(**payment_data_pending)
    assert exc_info.value.errors() == [
        {"loc": ("__root__",), "msg": "Missing required parameter shopper_result_url", "type": "value_error"}
    ]


def test_payment_create_refund_payment_missing_payment_unique_id(payment_data_pending):
    debit = payment_data_pending
    debit["payment_type"] = "RF"
    with pytest.raises(ValidationError) as exc_info:
        schemas.PaymentCreate(**debit)
    assert exc_info.value.errors() == [
        {"loc": ("__root__",), "msg": "Missing required parameter payment_unique_id", "type": "value_error"}
    ]


def test_payment_create_fail():
    with pytest.raises(ValidationError) as exc_info:
        schemas.PaymentCreate()

    assert exc_info.value.errors() == [
        {"loc": ("unique_id",), "msg": "field required", "type": "value_error.missing"},
        {"loc": ("amount",), "msg": "field required", "type": "value_error.missing"},
        {"loc": ("currency",), "msg": "field required", "type": "value_error.missing"},
        {
            "loc": ("payment_brand",),
            "msg": "field required",
            "type": "value_error.missing",
        },
        {
            "loc": ("payment_type",),
            "msg": "field required",
            "type": "value_error.missing",
        },
        {
            "loc": ("notification_url",),
            "msg": "field required",
            "type": "value_error.missing",
        },
        {
            "loc": ("status_code",),
            "msg": "field required",
            "type": "value_error.missing",
        },
    ]


def test_payment_get(db_session, payment_data_pending):
    obj_in = schemas.PaymentCreate(**payment_data_pending)
    payment1: Payment = actions.payment.create(db_session, obj_in=obj_in)
    payment2: Payment = actions.payment.get(db_session, id=payment1.id)
    assert payment1 == payment2


def test_payment_get_by_unique_id(db_session, payment_data_pending):
    obj_in = schemas.PaymentCreate(**payment_data_pending)
    payment1: Payment = actions.payment.create(db_session, obj_in=obj_in)
    payment2: Payment = actions.payment.get_by_unique_id(db_session, payment_data_pending["unique_id"])
    assert payment1 == payment2


def test_webhook_get_by_unique_id(db_session, payment_data_pending, webhook_data):
    obj_in = schemas.PaymentCreate(**payment_data_pending)
    payment1: Payment = actions.payment.create(db_session, obj_in=obj_in)
    webhook_data["payment_id"] = payment1.id
    obj_in = schemas.WebhookCreate(**webhook_data)
    webhook: Webhook = actions.webhook.create(db_session, obj_in=obj_in)
    webhook2: Webhook = actions.webhook.get_by_unique_id(db_session, webhook_data["unique_id"])
    assert webhook == webhook2


def test_payment_list(db_session, payment_data_pending):
    unique_id = "a1202376c8d146728dac871d6f68b22"
    for i in range(0, 10):
        data = payment_data_pending.copy()
        data["unique_id"] = f"{unique_id}{i}"
        obj_in = schemas.PaymentCreate(**data)
        actions.payment.create(db_session, obj_in=obj_in)

    payments = actions.payment.get_all(db_session)
    assert len(payments) == 10
    for payment in payments:
        assert payment.unique_id.startswith(unique_id)


def test_payment_update(db_session, payment_data_pending):
    obj_start = schemas.PaymentCreate(**payment_data_pending)
    payment: Payment = actions.payment.create(db_session, obj_in=obj_start)

    update = payment_data_pending.copy()
    update["status_code"] = "100.100.100"
    obj_update = schemas.PaymentUpdate(**update)
    payment_updated: Payment = actions.payment.update(db_session, db_obj=copy(payment), obj_in=obj_update)
    assert payment_updated.status_code == "100.100.100"
    assert payment_updated.id == payment.id
    assert payment_updated is not payment
    assert payment_updated.status_code != payment.status_code


def test_status_create(db_session, payment_data_pending):
    payment: Payment = actions.payment.create(db_session, obj_in=schemas.PaymentCreate(**payment_data_pending))
    obj_in = schemas.StatusCreate(**payment_data_pending, payment_id=payment.id)
    status: Status = actions.status.create(db_session, obj_in=obj_in)
    assert status.payment_id == payment.id


def test_status_create_fail(db_session):
    with pytest.raises(ValidationError) as exc_info:
        schemas.StatusCreate()

    assert exc_info.value.errors() == [
        {
            "loc": ("payment_id",),
            "msg": "field required",
            "type": "value_error.missing",
        },
        {
            "loc": ("status_code",),
            "msg": "field required",
            "type": "value_error.missing",
        },
    ]


def test_status_get(db_session, payment_data_pending):
    payment: Payment = actions.payment.create(db_session, obj_in=schemas.PaymentCreate(**payment_data_pending))
    obj_in = schemas.StatusCreate(**payment_data_pending, payment_id=payment.id)
    status1: Status = actions.status.create(db_session, obj_in=obj_in)
    status2: Status = actions.status.get(db_session, id=status1.id)
    assert status1 == status2


def test_status_list(db_session, payment_data_pending):
    payment: Payment = actions.payment.create(db_session, obj_in=schemas.PaymentCreate(**payment_data_pending))
    for i in range(100, 200):
        data = dict(
            status_code=f"100.100.{i}",
            status_description=f"Some fake code description{i}",
            payment_id=payment.id,
        )
        actions.status.create(db_session, obj_in=schemas.StatusCreate(**data))

    statuses: Status = actions.status.get_all(db_session)
    assert len(statuses) == 100


def test_status_get_by_payment_id(db_session, payment_data_pending):
    payment: Payment = actions.payment.create(db_session, obj_in=schemas.PaymentCreate(**payment_data_pending))
    status1: Status = actions.status.create(
        db_session,
        obj_in=schemas.StatusCreate(**payment_data_pending, payment_id=payment.id),
    )
    data = payment_data_pending.copy()
    data["status_code"] = "100.100.100"
    data["status_description"] = "Some fake code"
    status2: Status = actions.status.create(db_session, obj_in=schemas.StatusCreate(**data, payment_id=payment.id))

    statuses: Status = actions.status.get_by_payment_id(db_session, payment.id)
    assert statuses == [status1, status2]
