from uuid import uuid4

import mock
import pytest
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from freezegun import freeze_time
from peachpayments_partner.fixtures import AMOUNT, PAYMENT_BRAND, TIMESTAMP, UNIQUE_ID
from peachpayments_partner.result_codes import result_codes

from app.internal import schemas as internal_schemas
from app.models import Payment
from app.redirect.actions import handle_redirect_response, validate_redirect


@pytest.fixture
def get_payment(payment_data_pending):
    def _get_payment(status=None):
        data = payment_data_pending
        if status is None:
            data["status_code"] = result_codes.TRANSACTION_PENDING.code
        else:
            data["status_code"] = status.code

        uuid = uuid4()
        data["id"] = uuid
        obj_in = jsonable_encoder(data)
        return Payment(**obj_in)  # type: ignore

    return _get_payment


@mock.patch("app.internal.actions.payment")
def test_validate_redirect_pass(payment_actions, get_payment):
    payment = get_payment()
    payment_actions.get.return_value = payment
    validate_redirect(None, payment.id)
    payment_actions.get.assert_called_once_with(None, payment.id)


@mock.patch("app.internal.actions.payment")
def test_validate_redirect_404(payment_actions):
    payment_actions.get.return_value = None
    uuid = uuid4()
    with pytest.raises(HTTPException) as exc_info:
        validate_redirect(None, payment_id=uuid)

    assert exc_info.value.status_code == 404
    assert exc_info.value.detail == "Payment not found"
    payment_actions.get.assert_called_once_with(None, uuid)


@mock.patch("app.internal.actions.payment")
def test_validate_redirect_422(payment_actions, get_payment):
    payment = get_payment(result_codes.TRANSACTION_SUCCEEDED)
    payment_actions.get.return_value = payment

    with pytest.raises(HTTPException) as exc_info:
        validate_redirect(None, payment_id=payment.id)

    assert exc_info.value.status_code == 422
    assert exc_info.value.detail == "Payment already processed"


test_data = [
    [True, result_codes.TRANSACTION_SUCCEEDED],
    [False, result_codes.TRANSACTION_DECLINED_FOR_UNKNOWN_REASON],
]


@pytest.mark.parametrize("should_pass,status", test_data)
@mock.patch("app.redirect.actions.validate_redirect")
@mock.patch("app.internal.actions.payment")
@mock.patch("app.internal.actions.status")
@mock.patch("app.redirect.actions.create_webhook")
@freeze_time(TIMESTAMP)
def test_handle_redirect_response(
    mock_create_webhook, status_actions, payment_actions, validate, should_pass, status, get_payment
):
    payment = get_payment()
    validate.return_value = payment
    payment_actions.update.return_value = payment
    response = handle_redirect_response(None, payment.id, should_pass)

    validate.assert_called_once_with(None, payment.id)
    payment_actions.update.assert_called_once_with(
        None,
        db_obj=payment,
        obj_in=dict(status_code=status.code),
    )
    status_actions.create.assert_called_once_with(
        None,
        obj_in=internal_schemas.StatusCreate(
            payment_id=payment.id,
            status_code=status.code,
            status_description=status.description,
        ),
    )
    mock_create_webhook.assert_called_once_with(
        None,
        {
            "uniqueId": UNIQUE_ID,
            "amount": AMOUNT,
            "currency": "ZAR",
            "paymentBrand": PAYMENT_BRAND,
            "paymentType": "DB",
            "result": {"code": "000.200.000"},
            "connectorTxID1": payment.id,
            "timestamp": TIMESTAMP,
        },
        payment,
        "https://peachnotify.com",
    )
    assert response.status_code == 302
    assert response.headers["location"] == f"{payment.shopper_result_url}?uniqueId={UNIQUE_ID}"
