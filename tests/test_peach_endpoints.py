import mock
import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from freezegun import freeze_time
from peachpayments_partner.exceptions import AuthError
from peachpayments_partner.fixtures import TIMESTAMP
from peachpayments_partner.result_codes import result_codes
from peachpayments_partner_pydantic import schemas
from sqlalchemy.orm import Session

from app.internal import actions as internal_actions
from app.internal import schemas as internal_schemas
from app.utils.auth import peach_auth_required
from app.utils.exception import APIException


@mock.patch("app.utils.auth.is_authenticated")
def test_get_transaction_status(mock_auth, app: FastAPI, db_session: Session, client: TestClient, payment_data_pending):
    mock_auth.return_value = True
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment = internal_actions.payment.create(db_session, obj_in=obj_in)
    response = client.get(f"/v1/status/{payment.unique_id}", headers={"Authorization": "Bearer test"})
    assert response.status_code == 200
    data = response.json()
    assert data["uniqueId"] == payment.unique_id


RESPONSE_OPTIONAL_FIELDS = [
    "resultDetails",
    "customParameters",
    "clearingInstituteSessionId",
    "recon",
    "bankAccount",
    "card",
]

REQUEST_OPTIONAL_FIELDS = [
    "customer",
    "customParameters",
    "merchantTransactionId",
    "merchantInvoiceId",
    "clearingInstituteSessionId",
    "card",
    "billing",
    "shipping",
    "cart",
]


def delete_optional_fields(data: dict, optional_fields: list[str]) -> dict:
    for field in optional_fields:
        if field in data:
            del data[field]

    return data


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
def test_get_minimal_transaction_status(
    mock_auth, app: FastAPI, db_session: Session, client: TestClient, payment_data_success, status_response
):
    mock_auth.return_value = True
    obj_in = internal_schemas.PaymentCreate(**payment_data_success)
    payment = internal_actions.payment.create(db_session, obj_in=obj_in)
    response = client.get(f"/v1/status/{payment.unique_id}", headers={"Authorization": "Bearer test"})
    assert response.status_code == 200
    data = response.json()
    status_response = delete_optional_fields(status_response, RESPONSE_OPTIONAL_FIELDS)
    status_response["connectorTxID1"] = str(payment.id)
    status_response["customParameters"] = {}
    assert data == status_response


@mock.patch("app.utils.auth.is_authenticated")
@freeze_time(TIMESTAMP)
def test_get_transaction_status_404(mock_auth, app: FastAPI, db_session: Session, client: TestClient):
    mock_auth.return_value = True
    with pytest.raises(APIException) as api_exception:
        client.get("/v1/status/a1202376c8d146728dac871d6f68b221", headers={"Authorization": "Bearer test"})

    assert api_exception.value.status == 404
    assert api_exception.value.custom_message == {
        "result": {"code": result_codes.CANNOT_FIND_TRANSACTION.code},
        "timestamp": TIMESTAMP,
    }


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_post_debit_success(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    debit_request,
    debit_response,
):
    mock_auth.return_value = True
    response = client.post("/v1/debit", json=debit_request, headers={"Authorization": "Bearer test"})
    assert response.status_code == 200
    data = response.json()
    assert schemas.DebitResponse(**data)
    debit_response["connectorTxID1"] = mock.ANY
    debit_response["redirect"] = dict(
        method="GET",
        parameters=[dict(name="payment_id", value=mock.ANY)],
        url="http://localhost:8008/redirect",
    )
    debit_response["result"]["code"] = result_codes.TRANSACTION_PENDING.code
    assert data == debit_response
    mock_create_webhook.assert_called_once_with(db_session, mock.ANY, mock.ANY, debit_request["notificationUrl"])


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_post_debit_success_with_post_redirect(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    debit_request,
    debit_response,
):
    mock_auth.return_value = True
    # Forcing redirect method with changing the amount to "0.99"
    debit_request["amount"] = "0.99"
    response = client.post("/v1/debit", json=debit_request, headers={"Authorization": "Bearer test"})
    assert response.status_code == 200
    data = response.json()
    assert schemas.DebitResponse(**data)
    debit_response["amount"] = "0.99"
    debit_response["connectorTxID1"] = mock.ANY
    debit_response["redirect"] = dict(
        method="POST",
        parameters=[dict(name="payment_id", value=mock.ANY)],
        url="http://localhost:8008/redirect",
    )
    debit_response["result"]["code"] = result_codes.TRANSACTION_PENDING.code
    assert data == debit_response
    mock_create_webhook.assert_called_once_with(db_session, mock.ANY, mock.ANY, debit_request["notificationUrl"])


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_post_debit_minimal_success(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    debit_request,
    debit_response,
):
    debit_request = delete_optional_fields(debit_request, REQUEST_OPTIONAL_FIELDS)
    debit_response = delete_optional_fields(debit_response, RESPONSE_OPTIONAL_FIELDS)
    mock_auth.return_value = True
    response = client.post("/v1/debit", json=debit_request, headers={"Authorization": "Bearer test"})
    assert response.status_code == 200
    data = response.json()
    assert schemas.DebitResponse(**data)
    debit_response["connectorTxID1"] = mock.ANY
    debit_response["redirect"] = dict(
        method="GET",
        parameters=[dict(name="payment_id", value=mock.ANY)],
        url="http://localhost:8008/redirect",
    )
    debit_response["result"]["code"] = result_codes.TRANSACTION_PENDING.code
    assert data == debit_response
    mock_create_webhook.assert_called_once_with(db_session, mock.ANY, mock.ANY, debit_request["notificationUrl"])


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
def test_post_debit_failed_payment_type(
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    debit_request,
    unique_id,
    payment_data_success,
):
    mock_auth.return_value = True
    internal_actions.payment.create(
        db=db_session,
        obj_in=internal_schemas.PaymentCreate(**payment_data_success),
    )
    debit_request["paymentType"] = "RF"
    response = client.post(
        "/v1/debit",
        json=debit_request,
        headers={"Authorization": "Bearer test"},
    )
    assert response.status_code == 400
    data = response.json()
    assert data == {
        "result": {
            "code": "200.300.404",
            "parameterErrors": [
                {"message": "Wrong payment type for the debit request", "name": "paymentType", "value": "RF"}
            ],
        },
        "timestamp": TIMESTAMP,
    }


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_post_refund_success(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    refund_request,
    refund_response,
    unique_id,
    payment_data_success,
):
    mock_auth.return_value = True
    internal_actions.payment.create(
        db=db_session,
        obj_in=internal_schemas.PaymentCreate(**payment_data_success),
    )
    response = client.post(
        f"/v1/refund/{unique_id}",
        json=refund_request,
        headers={"Authorization": "Bearer test"},
    )
    assert response.status_code == 200
    data = response.json()
    assert schemas.RefundResponse(**data)
    refund_response["connectorTxID1"] = mock.ANY
    assert data == refund_response
    mock_create_webhook.assert_called_once_with(db_session, mock.ANY, mock.ANY, refund_request["notificationUrl"])


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_post_minimal_refund_success(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    refund_request,
    refund_response,
    unique_id,
    payment_data_success,
):
    mock_auth.return_value = True
    payment_data_success = delete_optional_fields(payment_data_success, REQUEST_OPTIONAL_FIELDS)
    internal_actions.payment.create(
        db=db_session,
        obj_in=internal_schemas.PaymentCreate(**payment_data_success),
    )
    refund_request = delete_optional_fields(refund_request, REQUEST_OPTIONAL_FIELDS)
    response = client.post(
        f"/v1/refund/{unique_id}",
        json=refund_request,
        headers={"Authorization": "Bearer test"},
    )
    assert response.status_code == 200
    data = response.json()
    assert schemas.RefundResponse(**data)
    refund_response["connectorTxID1"] = mock.ANY
    refund_response = delete_optional_fields(refund_response, RESPONSE_OPTIONAL_FIELDS)
    assert data == refund_response
    mock_create_webhook.assert_called_once_with(db_session, mock.ANY, mock.ANY, refund_request["notificationUrl"])


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
def test_post_refund_failed_payment_type(
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    refund_request,
    unique_id,
    payment_data_success,
):
    mock_auth.return_value = True
    payment_data_success = delete_optional_fields(payment_data_success, REQUEST_OPTIONAL_FIELDS)
    internal_actions.payment.create(
        db=db_session,
        obj_in=internal_schemas.PaymentCreate(**payment_data_success),
    )
    refund_request["paymentType"] = "DB"
    response = client.post(
        f"/v1/refund/{unique_id}",
        json=refund_request,
        headers={"Authorization": "Bearer test"},
    )
    assert response.status_code == 400
    data = response.json()
    assert data == {
        "result": {
            "code": "200.300.404",
            "parameterErrors": [
                {"message": "Wrong payment type for the refund request", "name": "paymentType", "value": "DB"}
            ],
        },
        "timestamp": TIMESTAMP,
    }


@mock.patch("app.utils.auth.is_authenticated")
@freeze_time(TIMESTAMP)
def test_refund_404_transaction_not_found(
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    refund_request,
):
    mock_auth.return_value = True
    with pytest.raises(APIException) as api_exception:
        client.post("/v1/refund/c3892bcs92202bef93920", json=refund_request, headers={"Authorization": "Bearer test"})

    assert api_exception.value.status == 404
    assert api_exception.value.custom_message == {
        "result": {"code": result_codes.CANNOT_FIND_TRANSACTION.code},
        "timestamp": TIMESTAMP,
    }


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_refund_transaction_amount_not_equal(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    refund_request,
    unique_id,
    payment_data_pending,
    amount,
    payment_brand,
):
    mock_auth.return_value = True
    payment_data_pending["amount"] = "1.00"
    internal_actions.payment.create(
        db=db_session,
        obj_in=internal_schemas.PaymentCreate(**payment_data_pending),
    )

    response = client.post(
        f"/v1/refund/{unique_id}",
        json=refund_request,
        headers={"Authorization": "Bearer test"},
    )
    assert response.status_code == 200
    data = response.json()
    assert schemas.RefundResponse(**data)
    assert data == dict(
        amount=amount,
        connectorTxID1=mock.ANY,
        currency="ZAR",
        customParameters={"some key": "some value"},
        paymentBrand=payment_brand,
        paymentType="RF",
        result=dict(code="700.400.200"),
        uniqueId=unique_id,
        timestamp=TIMESTAMP,
    )
    mock_create_webhook.assert_called_once()


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_refund_transaction_status_not_success(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    refund_request,
    payment_data_pending,
    unique_id,
    amount,
    payment_brand,
):
    mock_auth.return_value = True
    payment_data_pending["status_code"] = result_codes.TRANSACTION_PENDING.code
    internal_actions.payment.create(
        db=db_session,
        obj_in=internal_schemas.PaymentCreate(**payment_data_pending),
    )

    response = client.post(
        f"/v1/refund/{unique_id}",
        json=refund_request,
        headers={"Authorization": "Bearer test"},
    )
    assert response.status_code == 200
    data = response.json()
    assert schemas.RefundResponse(**data)
    assert data == dict(
        amount=amount,
        connectorTxID1=mock.ANY,
        currency="ZAR",
        customParameters={"some key": "some value"},
        paymentBrand=payment_brand,
        paymentType="RF",
        result=dict(code="700.400.200"),
        uniqueId=unique_id,
        timestamp=TIMESTAMP,
    )
    mock_create_webhook.assert_called_once()


@mock.patch("app.utils.auth.is_authenticated")
def test_successful_authentication(mock_auth):
    assert peach_auth_required("token") is None


@mock.patch("app.utils.auth.is_authenticated")
@freeze_time(TIMESTAMP)
def test_unsuccessful_authentication(mock_auth):
    mock_auth.side_effect = AuthError("error")
    with pytest.raises(APIException) as api_exception:
        peach_auth_required("token")
    assert api_exception.value.status == 401
    assert api_exception.value.custom_message == {
        "result": {"code": result_codes.TRANSACTION_DECLINED_BY_AUTHORIZATION_SYSTEM.code},
        "timestamp": TIMESTAMP,
    }


@freeze_time(TIMESTAMP)
def test_status_unsuccessful_authentication(
    app: FastAPI, db_session: Session, client: TestClient, payment_data_pending
):
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    with pytest.raises(APIException) as api_exception:
        client.get(f"/v1/status/{obj_in.unique_id}", headers={"Authorization": "invalid_token"})

    assert api_exception.value.status == 401
    assert api_exception.value.custom_message == {
        "result": {"code": result_codes.TRANSACTION_DECLINED_BY_AUTHORIZATION_SYSTEM.code},
        "timestamp": TIMESTAMP,
    }


@freeze_time(TIMESTAMP)
def test_debit_unsuccessful_authentication(db_session: Session, client: TestClient, debit_request):
    with pytest.raises(APIException) as api_exception:
        client.post("/v1/debit", json=debit_request, headers={"Authorization": "invalid_token"})
    assert api_exception.value.status == 401
    assert api_exception.value.custom_message == {
        "result": {"code": result_codes.TRANSACTION_DECLINED_BY_AUTHORIZATION_SYSTEM.code},
        "timestamp": TIMESTAMP,
    }


@mock.patch("app.utils.auth.is_authenticated")
@freeze_time(TIMESTAMP)
def test_refund_unsuccessful_authentication(
    mock_auth, app: FastAPI, db_session: Session, client: TestClient, refund_request, unique_id
):
    mock_auth.side_effect = AuthError("error")
    with pytest.raises(APIException) as api_exception:
        client.post(
            f"/v1/refund/{unique_id}",
            json=refund_request,
            headers={"Authorization": "invalid_token"},
        )
    assert api_exception.value.status == 401
    assert api_exception.value.custom_message == {
        "result": {"code": result_codes.TRANSACTION_DECLINED_BY_AUTHORIZATION_SYSTEM.code},
        "timestamp": TIMESTAMP,
    }


@mock.patch("app.utils.auth.is_authenticated")
def test_post_debit_error(_, app: FastAPI, db_session: Session, client: TestClient, debit_request):
    del debit_request["amount"]
    response = client.post("/v1/debit", json=debit_request, headers={"Authorization": "Bearer test"})
    assert response.status_code == 400
    data = response.json()
    assert data["result"]["code"] == result_codes.INVALID_OR_MISSING_PARAMETER.code
    assert data["result"]["parameterErrors"][0]["name"] == "amount"
    assert data["result"]["parameterErrors"][0]["message"] == "Field required"


@mock.patch("app.utils.auth.is_authenticated")
@freeze_time(TIMESTAMP)
def test_cancel_404_transaction_not_found(
    mock_auth, app: FastAPI, db_session: Session, client: TestClient, payment_data_pending, cancel_request
):
    mock_auth.return_value = True
    internal_actions.payment.create(
        db=db_session,
        obj_in=internal_schemas.PaymentCreate(**payment_data_pending),
    )
    with pytest.raises(APIException) as api_exception:
        client.post("/v1/cancel/c3892bcs92202bef93932", json=cancel_request, headers={"Authorization": "Bearer test"})

    assert api_exception.value.status == 404
    assert api_exception.value.custom_message == {
        "result": {"code": result_codes.CANNOT_FIND_TRANSACTION.code},
        "timestamp": TIMESTAMP,
    }


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_cancel_200_successful_pending_transaction(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    payment_data_pending,
    cancel_request,
    cancel_response,
):

    mock_auth.return_value = True
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment = internal_actions.payment.create(db_session, obj_in=obj_in)

    response = client.post(
        f"/v1/cancel/{payment.unique_id}", json=cancel_request, headers={"Authorization": "Bearer test"}
    )

    assert response.status_code == 200
    data = response.json()
    assert data["uniqueId"] == payment.unique_id
    assert data["result"] == dict(code=result_codes.CANCELLED_BY_USER.code)

    assert schemas.CancelResponse(**data)
    cancel_response["connectorTxID1"] = mock.ANY
    cancel_response["paymentBrand"] = "PartnerBrand"
    assert data == cancel_response

    response = client.get(f"/v1/status/{payment.unique_id}", headers={"Authorization": "Bearer test"})
    assert response.status_code == 200
    data = response.json()
    assert data["uniqueId"] == payment.unique_id
    assert data["result"] == dict(code=result_codes.CANCELLED_BY_USER.code)
    mock_create_webhook.assert_called_once_with(db_session, mock.ANY, mock.ANY, cancel_request["notificationUrl"])


@freeze_time(TIMESTAMP)
@mock.patch("app.utils.auth.is_authenticated")
@mock.patch("app.peach.actions.create_webhook")
def test_cancel_200_minimal_successful_pending_transaction(
    mock_create_webhook,
    mock_auth,
    app: FastAPI,
    db_session: Session,
    client: TestClient,
    payment_data_pending,
    cancel_request,
    cancel_response,
):

    payment_data_pending = delete_optional_fields(payment_data_pending, REQUEST_OPTIONAL_FIELDS)
    cancel_request = delete_optional_fields(cancel_request, REQUEST_OPTIONAL_FIELDS)
    cancel_response = delete_optional_fields(cancel_response, RESPONSE_OPTIONAL_FIELDS)
    mock_auth.return_value = True
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment = internal_actions.payment.create(db_session, obj_in=obj_in)

    response = client.post(
        f"/v1/cancel/{payment.unique_id}", json=cancel_request, headers={"Authorization": "Bearer test"}
    )

    assert response.status_code == 200
    data = response.json()
    assert data["uniqueId"] == payment.unique_id
    assert data["result"] == dict(code=result_codes.CANCELLED_BY_USER.code)

    assert schemas.CancelResponse(**data)
    cancel_response["connectorTxID1"] = mock.ANY
    cancel_response["paymentBrand"] = "PartnerBrand"
    assert data == cancel_response

    response = client.get(f"/v1/status/{payment.unique_id}", headers={"Authorization": "Bearer test"})
    assert response.status_code == 200
    data = response.json()
    assert data["uniqueId"] == payment.unique_id
    assert data["result"] == dict(code=result_codes.CANCELLED_BY_USER.code)
    mock_create_webhook.assert_called_once_with(db_session, mock.ANY, mock.ANY, cancel_request["notificationUrl"])


@mock.patch("app.utils.auth.is_authenticated")
@freeze_time(TIMESTAMP)
def test_cancel_400_transaction(
    mock_auth, app: FastAPI, db_session: Session, client: TestClient, payment_data_success, cancel_request
):
    mock_auth.return_value = True
    obj_in = internal_schemas.PaymentCreate(**payment_data_success)
    payment = internal_actions.payment.create(db_session, obj_in=obj_in)

    with pytest.raises(APIException) as api_exception:
        client.post(f"/v1/cancel/{payment.unique_id}", json=cancel_request, headers={"Authorization": "Bearer test"})
    assert api_exception.value.status == 400
    assert api_exception.value.custom_message == {
        "result": {
            "code": result_codes.INVALID_TRANSACTION_FLOW_THE_REQUESTED_FUNCTION_IS_NOT_APPLICABLE_FOR_THE_REFERENCED_TRANSACTION.code,
        },
        "timestamp": TIMESTAMP,
    }
