import mock
from fastapi import FastAPI
from fastapi.testclient import TestClient
from peachpayments_partner.fixtures import SHOPPER_RESULT_URL, UNIQUE_ID
from peachpayments_partner.result_codes import result_codes
from pydantic import UUID4
from sqlalchemy.orm import Session

from app.internal import actions as internal_actions
from app.internal import schemas as internal_schemas
from app.models import Payment


def test_display_checkout_page(db_session: Session, client: TestClient, payment_data_pending):
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment: Payment = internal_actions.payment.create(db_session, obj_in=obj_in)
    response = client.get(f"/redirect/?payment_id={payment.id}")
    assert response.status_code == 200
    assert f'<input type="hidden" name="payment_id" value="{payment.id}">' in response.text


def test_display_checkout_page_post(db_session: Session, client: TestClient, payment_data_pending):
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment: Payment = internal_actions.payment.create(db_session, obj_in=obj_in)
    response = client.post("/redirect", data=dict(payment_id=payment.id))
    assert response.status_code == 200
    assert f'<input type="hidden" name="payment_id" value="{payment.id}">' in response.text


def test_display_checkout_page_404(app: FastAPI, db_session: Session, client: TestClient):
    response = client.get("/redirect/?payment_id=64003abb-b675-43d9-a8f4-c5e116bf9dc6")
    assert response.status_code == 404


def test_display_checkout_page_422(app: FastAPI, db_session: Session, client: TestClient, payment_data_pending):
    payment_data_pending["status_code"] = result_codes.TRANSACTION_SUCCEEDED.code
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment = internal_actions.payment.create(db_session, obj_in=obj_in)
    response = client.get(f"/redirect/?payment_id={payment.id}")
    assert response.status_code == 422


@mock.patch("app.redirect.actions.create_webhook")
def test_handle_checkout_response_pass(
    mock_create_webhook, db_session: Session, client: TestClient, payment_data_pending
):
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment: Payment = internal_actions.payment.create(db_session, obj_in=obj_in)
    status = result_codes.TRANSACTION_SUCCEEDED

    response = client.post(
        "/redirect_response", data=dict(payment_id=payment.id, should_pass=True), follow_redirects=False
    )
    assert response.status_code == 302
    assert response.headers["location"] == f"{SHOPPER_RESULT_URL}?uniqueId={UNIQUE_ID}"

    uuid: UUID4 = payment.id  # type: ignore
    payment2 = internal_actions.payment.get(db_session, id=uuid)
    assert payment2
    assert payment2.status_code == status.code

    statuses = internal_actions.status.get_by_payment_id(db_session, uuid)
    assert len(statuses) == 1
    assert statuses[0].status_code == status.code
    mock_create_webhook.assert_called_once()


@mock.patch("app.redirect.actions.create_webhook")
def test_handle_checkout_response_fail(
    mock_create_webhook, db_session: Session, client: TestClient, payment_data_pending
):
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment: Payment = internal_actions.payment.create(db_session, obj_in=obj_in)
    status = result_codes.TRANSACTION_DECLINED_FOR_UNKNOWN_REASON

    response = client.post(
        "/redirect_response", data=dict(payment_id=payment.id, should_pass=False), follow_redirects=False
    )

    assert response.status_code == 302
    assert response.headers["location"] == f"{SHOPPER_RESULT_URL}?uniqueId={UNIQUE_ID}"
    uuid: UUID4 = payment.id  # type: ignore
    payment2 = internal_actions.payment.get(db_session, id=uuid)
    assert payment2
    assert payment2.status_code == status.code
    statuses = internal_actions.status.get_by_payment_id(db_session, uuid)
    assert len(statuses) == 1
    assert statuses[0].status_code == status.code
    mock_create_webhook.assert_called_once()


def test_display_checkout_response_404(app: FastAPI, db_session: Session, client: TestClient):
    response = client.post(
        "/redirect_response",
        data=dict(payment_id="64003abb-b675-43d9-a8f4-c5e116bf9dc6", should_pass=True),
    )
    assert response.status_code == 404


@mock.patch("app.redirect.actions.create_webhook")
def test_display_checkout_response_422(
    mock_create_webhook, app: FastAPI, db_session: Session, client: TestClient, payment_data_pending
):
    payment_data_pending["status_code"] = result_codes.TRANSACTION_SUCCEEDED.code
    obj_in = internal_schemas.PaymentCreate(**payment_data_pending)
    payment = internal_actions.payment.create(db_session, obj_in=obj_in)
    response = client.post("/redirect_response", data=dict(payment_id=payment.id, should_pass=False))
    assert response.status_code == 422
