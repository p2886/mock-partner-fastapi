import json
import os
from copy import deepcopy
from typing import Any, Generator

import pytest
from dateutil import parser
from fastapi import FastAPI
from fastapi.testclient import TestClient
from peachpayments_partner.fixtures import (
    AMOUNT,
    CANCEL_REQUEST,
    CANCEL_RESPONSE,
    CURRENCY,
    DEBIT_REQUEST,
    DEBIT_RESPONSE,
    NOTIFICATION_URL,
    PAYMENT_BRAND,
    REFUND_REQUEST,
    REFUND_RESPONSE,
    SHOPPER_RESULT_URL,
    STATUS_RESPONSE,
    TIMESTAMP,
    UNIQUE_ID,
    WEBHOOK_REQUEST,
)
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.config import settings
from app.db.base import Base
from app.db.session import get_db
from app.main import get_application

from .fixtures import PAYMENT_DATA_PENDING, PAYMENT_DATA_SUCCESS

DATABASE_URL = os.getenv(
    "TEST_DATABASE_URL",
    settings.assemble_db_connection(
        None,
        values=dict(
            POSTGRES_USER=settings.POSTGRES_USER,
            POSTGRES_PASSWORD=settings.POSTGRES_PASSWORD,
            POSTGRES_SERVER="localhost",
            POSTGRES_PORT=settings.POSTGRES_TEST_PORT,
            POSTGRES_DB=settings.POSTGRES_DB,
        ),
    ),
)
engine = create_engine(DATABASE_URL)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@pytest.fixture
def auth_token():
    return (
        "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXJ0bmVyIjp0cn"
        "VlLCJzaWQiOiJkN2ZiOWJhYjFlYmJhNzg2YzM3YiIsImlhdCI6MTYzMDg2MzQyMy"
        "wibmJmIjoxNjMwODYzNDIzLCJleHAiOjE2MzA4Nzc4MjMsImF1ZCI6Imh0dHBzOi8"
        "vbTJtLnBlYWNocGF5bWVudHMuY29tIiwiaXNzIjoiaHR0cHM6Ly9xYS5hdXRoZW50a"
        "WNhdGlvbi5wZWFjaHBheW1lbnRzLmNvbS8iLCJzdWIiOiI1MjRlMTc4MDcyZjA5YzI5Z"
        "DA0N2UzYzU4NjliZDgifQ.mW5FthvQN0RxR800JFbia1CSmp8Ehrx5ZySs-044EBFCBcj"
        "lA4m_9pd_PcOHHP6wBxc3blVvcwLJ_JdtYVQ95NaQHmKGgPeyWpfZr8Q2yYm9vM_WFRhY9H"
        "xc9quBqjks3ZDJ56GHSHWEHSafoHYaZ0XPvvefyt3XvFzPutk7xX7fFE6r7pzo9HV3fN4Q1_e"
        "-ln9CtxZpeQCWI60LDEl5mfbvxFLemm1BOZCiLYo-aTEjnkf3Dmpj4OPaJfmf-y4TDE_3Pw-wf"
        "8Vh9Psw_r6aDHIvB2XuV3Kz1C3KM9f2dHZyL7gPrUEJY8I7GV0OCwIy1TLBEKSTaRCsjYFhZrmH"
        "rKSky-PGr0JIb5yKtoUec_MC7U04USuoCe4jbo9lp6W32ND9SYB3FhfpVC-ughFMeua8GD4ra"
        "CNVOEBM1YE8xoTop5D7AJLtiSngWeDReEiZpZIntismqr4iHC9DM6OeLJV8vWXCo7Vt7tm_18"
        "lk0AgYxx2XhgWYH8lDKFeglCIg44HwdWXwDyBYSOfYTph4crDfC3kspCXZ7e3ZOPlTThcif8ZK"
        "WlxNCaiTN20CBhqGP1VEFCTRhz-8Eq7lZiin-j0zqlq-GVrXerO59YgMzwNZsBqUupUuGrEGyikr"
        "DF4gZYkZ4iUyNKbjlZO0j6Sz3FcsHJcL"
    )


@pytest.fixture()
def app() -> Generator[FastAPI, Any, None]:
    """Create a fresh database on each test case."""
    # Create tables
    Base.metadata.create_all(engine)  # type: ignore
    _app = get_application()
    # Use app in tests
    yield _app
    # Delete all data
    Base.metadata.drop_all(engine)  # type: ignore


@pytest.fixture
def db_session(app: FastAPI) -> Generator[Any, Any, None]:
    """Creates a fresh sqlalchemy session for each test.

    The transaction is rolled back at the end of each test ensuring a clean state.
    """
    connection = engine.connect()
    transaction = connection.begin()
    session = Session(bind=connection)
    # use the session in tests.
    yield session
    session.close()
    # rollback
    transaction.rollback()
    connection.close()


@pytest.fixture()
def client(app: FastAPI, db_session: Any) -> Generator[TestClient, Any, None]:
    """Create a new FastAPI TestClient.

    It uses the `db_session` fixture to override the `get_db` dependency
    that is used in main.
    """

    def _get_test_db():
        yield db_session

    app.dependency_overrides[get_db] = _get_test_db
    with TestClient(app) as client:
        yield client


@pytest.fixture
def webhook_data(webhook_request):
    return {
        "payment_id": "cc392a60-994f-4c6e-8717-672129229c19",
        "unique_id": UNIQUE_ID,
        "data": json.dumps(webhook_request),
        "is_delivered": True,
        "notification_url": "https://example.com/webhook",
        "attempt": 1,
        "trigger_timestamp": 123456789,
    }


@pytest.fixture()
def amount():
    return AMOUNT


@pytest.fixture()
def currency():
    return CURRENCY


@pytest.fixture()
def unique_id():
    return UNIQUE_ID


@pytest.fixture()
def notification_url():
    return NOTIFICATION_URL


@pytest.fixture()
def shopper_result_url():
    return SHOPPER_RESULT_URL


@pytest.fixture()
def payment_brand():
    return PAYMENT_BRAND


@pytest.fixture
def debit_request():
    return deepcopy(DEBIT_REQUEST)


@pytest.fixture
def cancel_request():
    return deepcopy(CANCEL_REQUEST)


@pytest.fixture
def refund_request():
    return deepcopy(REFUND_REQUEST)


@pytest.fixture
def debit_response():
    return deepcopy(DEBIT_RESPONSE)


@pytest.fixture
def refund_response():
    return deepcopy(REFUND_RESPONSE)


@pytest.fixture
def cancel_response():
    return deepcopy(CANCEL_RESPONSE)


@pytest.fixture
def payment_data_success():
    return deepcopy(PAYMENT_DATA_SUCCESS)


@pytest.fixture
def payment_data_pending():
    return deepcopy(PAYMENT_DATA_PENDING)


@pytest.fixture
def status_response():
    return deepcopy(STATUS_RESPONSE)


@pytest.fixture
def timestamp():
    return parser.parse(TIMESTAMP)


@pytest.fixture
def webhook_request():
    return deepcopy(WEBHOOK_REQUEST)
