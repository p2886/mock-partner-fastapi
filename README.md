# Mock Partner - FastAPI

FastAPI implementation of a sample Payment Service Provider application.

## Summary

Start the app with `docker-compose up`. Swagger documentation can be found at `http://localhost:8008/docs`.
OpenAPI docs `http://localhost:8008/openapi.json`.

### Key terms

| Term                     | Definition                                                                                                         |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------ |
| Partner API              | A service provided by Peach Payments to enable Payment Service Providers to become available on the Peach Platform |
| Payment Service Provider | A payment service provider who integrates with the Partner API                                                     |
| Outbound API call        | API calls sent from Partner API to the Payment Service Provider                                                    |
| Inbound API call         | API calls sent from Payment Service Provider to Partner API                                                        |

This sample application serves as a showcase of a Payment Service Provider setup.

The `peach` directory defines the required outbound endpoints. The `webhooks` and `redirect` directories specify how the
Payment Service Provider should handle inbound calls to the Peach Partner Service.

The necessary endpoints for outbound calls are:

* `/v1/debit` to process debit transaction requests
* `/v1/refund/{peachTransactionId}` to process transaction refund requests
* `/v1/status/{peachTransactionId}` to allow querying for a transaction's status

PeachPayments provides a webhook endpoint to serve the inbound calls. The Payment Service Provider will call it to send any transaction
status change updates.

In the debit response, the Payment Service Provider will provide a redirect URL which will be used by Peach to load the
Payment Service Provider's UI responsible for capturing necessary information from the user.

Data validation for this app is done with Pydantic which is already being leveraged by FastAPI.

Authentication for inbound calls requires the Payment Service Provider to use the Peach Authentication service to receive a JWToken to be
used as a header. Alternatively, for outbound calls, PeachPayments offers a token validation service to verify that
requests are in fact coming from Peach.

## System architecture

System is divided into three parts - internal, peach and redirect.

* "Internal" is the Payment Service Provider side. It is connected to the database and provides information for the user, who is a
  potential Partner employee.

* "Peach" provides the endpoint which directly interact with PeachPayments.

* "Redirect" is for interaction between the Payment Service Provider and the target customer as well as the redirect back to PeachPayments.

* "Webhook" is for interaction between the Payment Service Provider and PeachPayments for sending any transaction status updates.

The application depends on the `peachpayments-partner` and `peachpayments-partner-pydantic` packages.

* `peachpayments-partner` provides authentication support and result codes

* `peachpayments-partner-pydantic` provides the Pydantic schemas for the data validation

### Internal

*Note: The definition or existence of this part is not required by Peach in the real app.*

There are two models stored in Partner's database - `Payment` and `Status`.

### Peach

*Note: Only the existence and the structure of the endpoints is defined by PeachPayments.*

PeachPayments requires `/v1/debit`, `/v1/refund` and `/v1/status/{peachTransactionId}` endpoints to be working.

Peach related code interacts with database through code defined in `internal/actions.py`.

#### Authentication

For all requests coming from PeachPayments to the above endpoints, PeachPayments offers an optional token validation
service to verify that the requests are coming from PeachPayments.

The Partner can use the `peach_auth_required` function defined in `utils/auth.py`, as a dependency in various routes, to
validate the JWT token received from PeachPayments. The Partner can also use the `get_key` function available in the
`peachpayments-partner` library in the `authentication.py` module to retrieve the JWK to be used to validate the token.

For outbound calls to PeachPayments (webhooks and redirects, which are described later), the Partner will need to send
the access token in the requests to PeachPayments. The `peachpayments-partner` library provides a utility function
`get_access_token` (available in `authentication.py`) to retrieve this access token from PeachPayments. This library
also provides another utility function `TokenFormatter.format` in the module `jwt_formatter.py`, to validate the JWT
token and format it in the way PeachPayments expects it in the `Authorization` header.

#### `POST /v1/debit`

This endpoint has to be implemented in order to process debit transaction requests. The endpoint that the partner will
need to expose is `https://<partner-host>/v1/debit`.

PeachPayments requests `/v1/debit` using `peachpayments-partner-pydantic.schemas.DebitRequest` format. The
`peach.actions.debit()` method is called and a new `Payment` is created in database with a `TRANSACTIION_PENDING`
status. It is then translated to the format required by Peach API -
`peachpayments-partner-pydantic.schemas.DebitResponse` - and returned in JSON format.

One of the fields required to return to PeachPayments is the redirect information. It contains the method and the URL
that will host the UI for the payment method. Here it is implemented under the `/redirect` endpoint.

#### `POST /v1/refund/{peachTransactionId}`

This endpoint has to be implemented to handle refund request from Peach. The endpoint that the partner will need to
expose is `https://<partner-host>/v1/refund/{peachTransactionId}`.

Peach Payments requests `/v1/refund/{peachTransactionId}` using
`peachpayments-partner-pydantic.schemas.RefundRequest` format. The example given supports full refund but can be
adjusted to support partial refunds as well.

The `peach.actions.refund()` method is called and the first check that is done is to retrieve the `Payment` by the
`peachTransactionId` which is the id of the transaction to be refunded. An HTTP 404 error will be returned if no object
has been found. The other checks done are on the amount which should equal the initial transaction amount, the
paymentType of the transaction should be `DB` and finally the result codes which should be one of the success result
codes.

Once all the checks have passed the paymentType is updated to `RF`, the `refund_unique_id` is updated with the uniqueId
in the request and finally a new status is created. A refund response will be returned using
`peachpayments-partner-pydantic.schemas.RefundResponse`.

#### `GET /v1/status/{peachTransactionId}`

PeachPayments requires partner to implement this endpoint in order to allow Peach to query the transaction status at any
given point in the transaction lifecycle. The endpoint that the partner will need to expose is
`https://{partner-host}/v1/status/{peachTransactionId}`.

The transaction status stored in the database is retrieved and returned to as a JSON response defined in
`peachpayments-partner-pydantic.schemas.StatusResponse`.

### Webhook

Webhook functionality has to be implemented to send webhooks to Peach at different stages of the transaction's
lifecycle. An example of a transaction lifecycle change would be a payment going to a pending state after being
initiated [`peach.actions.debit`](app/peach/actions.py#L128).

The webhook functionality runs asynchronously as a background task using FastAPI's BackgroundTasks however for real-life
systems a distributed task queue such as [Celery](https://docs.celeryproject.org/en/stable/) would be more suitable.

The Refund and Debit Requests from Peach will contain a `notificationUrl`field. It is the Peach's webhook endpoint that
can be used to send the request.

An exponential backoff strategy has been implemented using the function `handle_backoff`, the backoff handles cases when
status code response for the webhook is not an HTTP 204. The backoff function has been implemented to retry sending the
webhook in incremental intervals. It continues sending the webhook until a successful status code has been received or,
the maximum retries have been reached. A webhook backoff object gets created to keep track of the retries and gets
deleted when one of the two retry termination conditions has been met.

### Redirect

The redirect URL will be used by PeachPayments to load the page containing input fields to capture information from the
user to process the transaction (e.g. login credentials, QR code, card details, etc.)

All further interaction with the user, e.g. multiple steps in a netbanking flow or capturing second-factor
authentication information like OTP, will continue in the Partner’s UI screens.

Once the transaction flow is completed, the Partner will redirect back to the PeachPayments service endpoint with the
final transaction status. PeachPayments will process the transaction status and will redirect back to Checkout, which
will be re-rendered and will show the success/error screen.

#### `GET /redirect?payment_id={peachTransactionId}`

The URL is provided to PeachPayments in a debit response. It creates an HTML page displayed to the customer for
additional authentication. Here we created a `<form>`, so the customer is able to choose if transaction should be
successful or declined. The form is submitted to the `/redirect_response` endpoint.

#### `POST /redirect_response`

The endpoint collects the response from the form filled by the customer, then it analyses it and stores the new
transaction status. Then the request is redirected to PeachPayments to show the result to the customer.

## Deployment

### On any virtual machine

As long as the database will be hidden behind the firewall you can use the suite running from `docker-compose`

* Create VM with exported port 80

* Install `docker-compose`

* Make git work (install, add [SSH key](https://gitlab.com/-/profile/keys), etc.)

* Clone the repository `git clone git@gitlab.com:peachpayments/mock-partner-fastapi.git`

* Copy `default.env` to `.env` and adjust the settings, especially `AUTH0_AUDIENCE`, `PEACH_TOKEN` and `HOST`.

* Run the suite and apply migrations:

```shell
$ export POSTGRES_PORT=5432
$ docker-compose up -d web
$ docker-compose run --entrypoint bash partner
> alembic upgrade head
> ^D
```

* To display the logs:

```shell
$ docker-compose logs -f partner
partner-1  | INFO:     Will watch for changes in these directories: \['/code']
...
```

### On Google Cloud Platform

MockPartner is using the PostgreSQL database. You can install it on any machine. For creating a database in GCP follow
<https://cloud.google.com/sql/docs/postgres/quickstart>. Create the instance with a public IP address and allow access for
the partner service.

Copy the production database credentials to the `.env.production` file.

Build docker image `docker build --target production -t gcr.io/{YOUR_PROJECT_NAME}/{MOCKPARTNER_NAME} .`

Publish to the Google Container Registry `docker push gcr.io/{YOUR_PROJECT_NAME}/{MOCKPARTNER_NAME}`

Container will serve the API on port `8008`.

Create the GCR instance using pushed image.

Set the environment variable `POSTGRES_PORT`, `POSTGRES_SERVER`, `POSTGRES_USER`, `POSTGRES_PASSWORD` and `POSTGRES_DB`
to match database configuration.

If in trouble follow the [Google Cloud Run docs](https://cloud.google.com/run/docs/deploying)

## Development

App is managed with [poetry](https://python-poetry.org/).

### First run

#### Environment for docker

Copy the `default.env` file if you run the app for the first time.

```shell
cp default.env .env
```

#### Environment for development machine

To use the database from local machine on tests and migrations export the environment variables:

```shell
export POSTGRES_PORT=5499
export POSTGRES_TEST_PORT=5498
export POSTGRES_SERVER=localhost
```

I use [direnv](https://direnv.net/) to automate this per directory.

Switch to python virtual envionment:

```shell
poetry shell
```

Install dependencies:

```shell
poetry install
```

#### Prepare the database

You can use the [`docker-compose`](https://docs.docker.com/compose/start/) to start the service.

```shell
docker-compose up -d partner
```

**Note:** As an alternative the Payment Service Provider can also create the database in different way and modify the environment variables. These are
`POSTGRES_USER`, `POSTGRES_PASSWORD`, `POSTGRES_SERVER`, `POSTGRES_DB`.

Apply migrations:

```shell
alembic upgrade head
```

Navigate to `http://127.0.0.1:8008`

Uvicorn is started with `--reload` mode. It will reload on local file changes.

### Testing

**Note:** Test database needs to be running for tests to pass.

```shell
docker-compose up -d test-database
```

```shell
pytest
```

To test with coverage run:

```shell
pytest --cov=app --cov-report term-missing
```

#### Partner shell

To access the shell in the "partner" container:

```shell
docker-compose run --entrypoint bash partner
```

#### Database

Database is served from `localhost` on port defined by environment variable `POSTGRES_PORT` and inside the docker
network from host `database` on port `5432`. To access the database from the developer machine:

```shell
psql postgresql://partner_user:partner_password@localhost:5499/partner_database
```

### Migrations

To automatically create a new migration:

```shell
alembic revision --autogenerate -m "Description of the change"
```

To upgrade to the latest migration:

```shell
alembic upgrade head
```

Migration file will be stored in `migrations` directory

To upgrade the test database to the latest migration one needs to run the following commands:

```shell
export OLD_POSTGRES_PORT=$POSTGRES_PORT
export POSTGRES_PORT=$POSTGRES_TEST_PORT
alembic upgrade head
export POSTGRES_PORT=$OLD_POSTGRES_PORT
```

### Dependencies

Add a library

```sh
poetry add package-name
```

Add a library for development

```sh
poetry add dev-package-name --dev
```

Remove a library

```sh
poetry remove package-name
```

After dependencies list is changed the docker needs to be rebuilt.:

```sh
docker-compose up partner --build
```

### Project directory structure

Most important files and directories:

```tree
.
├── README.md
├── app/
│   ├── config.py
│   ├── internal/
│   │   # Mock Partner internal API code
│   │   ├── actions.py
│   │   │   # REST for retrieving stored payments and statuses
│   │   ├── schemas.py
│   │   │   # definitions of the request and response objects
│   │   └── router.py
│   │       # definitions of the endpoints
│   ├── main.py
│   │   # app definition
│   ├── routes.py
│   │   # collecting all routers
│   ├── models.py
│   │   # database models
│   ├── peach/
│   │   # interact with PeachPayments
│   │   ├── actions.py
│   │   └── router.py
│   ├── redirect/
│   │   # PeachPayments will call this endpoint to redirect to the partner's UI
│   │   ├── actions.py
│   │   ├── schemas.py
│   │   ├── router.py
│   │   └── templates.py
│   ├── utils/
|   │    # exceptions definition and error response formatter
│   └── webhooks/
│       # contains Mock Partner API code to send webhooks to PeachPayments
│       └── actions.py
├── migrations/
└── tests/
```
