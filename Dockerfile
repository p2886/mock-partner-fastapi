FROM python:3.9-slim as base

ARG TARGET

ENV TARGET=${TARGET} \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.8.4

# System deps:
RUN pip install "poetry==$POETRY_VERSION"

# Add compilation capabilities for psycopg2
RUN apt-get update \
 && apt-get install apt-utils build-essential libpq-dev gcc --yes \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*


# Copy only requirements to cache them in docker layer
WORKDIR /code
COPY poetry.lock pyproject.toml /code/

# Creating folders, and files for a project:
COPY . /code

ENTRYPOINT uvicorn app.main:app --host 0.0.0.0 --port 8008

FROM base as development

# Project initialization:
RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi

FROM base as production

# Project initialization:
RUN poetry config virtualenvs.create false \
  && poetry install --no-dev --no-interaction --no-ansi

COPY .env.production /code/.env
