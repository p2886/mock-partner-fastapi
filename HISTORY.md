# History

## 0.1.3

-   Update `peachpayments-partner-pydantic` to version `0.2.5`.
    -   `0.00` is not allowed value for the `Amount`
-   Update `peachpayments-partner` to version 0.1.8.
    -   Change Peach auth service URL to live endpoint
-   Changes in the Webhook scheduler



## 0.1.2

-   Update `peachpayments-partner-pydantic` to version `0.2.4`