"""Utils for authentication."""

import logging
from typing import Optional

from fastapi import Depends
from fastapi import status as http_status
from fastapi.openapi.models import SecurityBase as SecurityBaseModel
from fastapi.security.base import SecurityBase
from peachpayments_partner.authentication import is_authenticated
from peachpayments_partner.error_response import format_error_response
from peachpayments_partner.exceptions import AuthError
from peachpayments_partner.result_codes import result_codes
from starlette.requests import Request

from app.config import settings
from app.utils.exception import APIException

logger = logging.getLogger(__name__)


class JWTAuth(SecurityBase):
    """Provide a special authorization where the token is provided in the Authorization header."""

    def __init__(self):
        self.scheme_name = "JWT Token"
        self.model = SecurityBaseModel(type="http", in_="header")

    async def __call__(self, request: Request) -> Optional[str]:
        """Called from dependency of the peach_auth_required.

        Args:
            request: The request object.

        Returns:
            The authorization token.

        Raises:
            APIException: If the authorization header is not provided.
        """
        authorization: str = request.headers.get("Authorization")
        if not authorization:
            logger.error("Authorization header not provided.")
            error_response = format_error_response(result_codes.TRANSACTION_DECLINED_BY_AUTHORIZATION_SYSTEM)
            raise APIException(custom_message=error_response, status=http_status.HTTP_401_UNAUTHORIZED)
        return authorization


oauth2_scheme = JWTAuth()


def peach_auth_required(token: str = Depends(oauth2_scheme)):
    """Dependency for the endpoints targeted by PeachPayments

    Args:
        token: The authorization token.

    Returns:
        None

    Raises:
        APIException: If failed to authenticate.
    """
    try:
        is_authenticated(token, audience=settings.AUTH0_AUDIENCE)
    except AuthError:
        logger.exception("Failed to authenticate.")
        error_response = format_error_response(result_codes.TRANSACTION_DECLINED_BY_AUTHORIZATION_SYSTEM)
        raise APIException(custom_message=error_response, status=http_status.HTTP_401_UNAUTHORIZED)
