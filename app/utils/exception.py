class APIException(Exception):
    def __init__(self, custom_message: dict, status: int):
        self.custom_message = custom_message
        self.status = status
