"""SQLAlchemy models definition."""
from uuid import uuid4

from sqlalchemy import TIMESTAMP, Boolean, Column, ForeignKey, Integer, String, Text, func
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.orm.relationships import RelationshipProperty

from .db.base import Base


class Payment(Base):
    """Payment database representation."""

    id = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)
    unique_id = Column(String)
    payment_unique_id = Column(String)
    amount = Column(String)
    currency = Column(String)
    payment_brand = Column(String)
    payment_type = Column(String)
    merchant_name = Column(String)
    merchant_transaction_id = Column(String)
    merchant_invoice_id = Column(String)
    notification_url = Column(String)
    shopper_result_url = Column(String)
    status_code = Column(String)
    error_message = Column(Text)
    created_at = Column(TIMESTAMP, server_default=func.now())
    updated_at = Column(TIMESTAMP, onupdate=func.now())
    clearing_institute_session_id = Column(String)
    custom_parameters = Column(Text)


class Status(Base):
    """Status database representation."""

    id = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)
    payment_id = Column(UUID(as_uuid=True), ForeignKey(Payment.id))
    status_code = Column(String)
    status_description = Column(String)
    error_message = Column(Text)
    created_at = Column(TIMESTAMP, server_default=func.now())
    payment: RelationshipProperty = relationship("Payment", foreign_keys="Status.payment_id")


class Webhook(Base):
    id = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)

    payment_id = Column(UUID(as_uuid=True), ForeignKey(Payment.id))
    # Peach unique_id
    unique_id = Column(String)
    # Data contains a JSON object which will instantiate the WebhookRequest model
    data = Column(String)
    # has webhook been delivered
    is_delivered = Column(Boolean)
    # has webhook been failed
    is_failed = Column(Boolean)
    # URL which will be used to send the WebhookRequest
    notification_url = Column(String(256))
    # Current attempt number
    attempt = Column(Integer, default=0)
    # When should we send the WebhookRequest again
    trigger_timestamp = Column(Integer, default=None)
    created_at = Column(TIMESTAMP, server_default=func.now())

    payment: RelationshipProperty = relationship("Payment", foreign_keys="Webhook.payment_id")
