"""Define the main API router."""

from fastapi import APIRouter

from .internal import router as internal_router
from .peach import router as peach_router
from .redirect import router as redirect_router

router = APIRouter()
router.include_router(peach_router.router)
router.include_router(redirect_router.router)
router.include_router(internal_router.router)
