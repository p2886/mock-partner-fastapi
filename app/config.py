"""Main app configuration."""

from typing import Any, Dict, Optional

from pydantic import BaseSettings, HttpUrl, PostgresDsn, validator


class Settings(BaseSettings):
    """App settings."""

    POSTGRES_SERVER: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    POSTGRES_TEST_PORT: str
    POSTGRES_PORT: str = "5432"
    HOST: str
    REDIRECT_URL: str

    MAX_WEBHOOK_ATTEMPT: int = 720

    PEACH_REDIRECT_ENDPOINT: str = "redirect"
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None
    AUTH0_AUDIENCE: str
    CLIENT_ID: str
    CLIENT_SECRET: str
    AUTH_SERVICE_GENERATE_ACCESS_TOKEN_URL: HttpUrl

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        """Assemble database connection string."""
        if isinstance(v, str):
            return v

        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            port=values.get("POSTGRES_PORT"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    class Config:
        """Additional configuration for the Settings."""

        case_sensitive = True
        env_file = ".env"


settings = Settings()
