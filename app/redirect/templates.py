"""Defines the HTML templates used in redirect flow."""

REDIRECT_HTML = """\
<html>
<body>
  <h2>Sample checkout page for payment $payment_id.</h2>
  <form action="/redirect_response/" method="post" target="_top">
    <input type="hidden" name="payment_id" value="$payment_id">
    <div>
      <input type="checkbox" name="should_pass">
      <label for="should_pass">Check in for success</label>
    </div>
    <div>
      <input type="submit" value="SUBMIT">
    </div>
  </form>
  <script>
    function post(status) {
      window.parent.postMessage(
        {
          message: "push-checkout-events",
          events: [status],
        },
        "https://testsecure.ppay.io",
        []
      );
    }

    post({ type: "status", value: "initializing" });

    setTimeout(() => {
      post({ type: "status", value: undefined });
    }, 1000);
  </script>
</body>
</html>\
"""
