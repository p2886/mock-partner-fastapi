"""Definition of redirect endpoints."""

from string import Template

from fastapi import APIRouter, Depends, Form
from fastapi.responses import HTMLResponse, RedirectResponse
from pydantic import UUID4
from sqlalchemy.orm import Session

from app.db.session import get_db

from .actions import handle_redirect_response, validate_redirect
from .templates import REDIRECT_HTML

router = APIRouter(tags=["redirect"])


# Note:
#     It's not required for the Partner to use both POST and GET redirect methods.
#     We provided both methods for integral testing.
#     POST request is used for debit transactions with amount equal 0.99.


@router.get("/redirect", response_class=HTMLResponse)
def redirect_get(*, db: Session = Depends(get_db), payment_id: UUID4) -> HTMLResponse:
    r"""Process the GET redirect request.

    Displays the Partner's non-synchronous flow page containing the form.

    Args:
        db: (Session) SQLAlchemy session.
        payment_id: (UUID4) Payment ID.

    Returns:
        (HTMLResponse) Redirect response.
    """
    validate_redirect(db, payment_id=payment_id)
    return HTMLResponse(
        content=Template(REDIRECT_HTML).substitute(payment_id=payment_id),
        status_code=200,
    )


@router.post("/redirect", response_class=HTMLResponse)
def redirect_post(*, db: Session = Depends(get_db), payment_id: UUID4 = Form(...)) -> HTMLResponse:
    r"""Process the POST redirect request.

    Displays the Partner's non-synchronous flow page containing the form.

    Args:
        db: (Session) SQLAlchemy session.
        payment_id: (UUID4) Payment ID.

    Returns:
        (HTMLResponse) Redirect response.
    """
    validate_redirect(db, payment_id=payment_id)
    return HTMLResponse(
        content=Template(REDIRECT_HTML).substitute(payment_id=payment_id),
        status_code=200,
    )


@router.post("/redirect_response", response_class=RedirectResponse, response_model_exclude_none=True)
def redirect_response(
    *,
    db: Session = Depends(get_db),
    payment_id: UUID4 = Form(...),
    should_pass: bool = Form(False),
) -> RedirectResponse:
    r"""Process the user response.

    Reads the response from the user stores the transaction status and redirects to the final destination.

    Args:
        db: (Session) SQLAlchemy session.
        payment_id: (UUID4) Payment ID.
        should_pass: (bool) Whether the response should pass.

    Returns:
        (RedirectResponse) Redirect response.
    """
    return handle_redirect_response(db, payment_id, should_pass)
