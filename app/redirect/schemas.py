"""Definitions of the [pydantic models](https://pydantic-docs.helpmanual.io/usage/models/).

These models will be used to validate the incoming request body and more
"""

from peachpayments_partner.result_codes import ResultCode
from peachpayments_partner_pydantic.schemas import TransactionResult
from pydantic import BaseModel

from app.models import Payment


class RedirectBack(BaseModel):
    """Redirect back to Peach after payment request has been processed by the Partner."""

    uniqueId: str
    amount: str
    currency: str
    paymentBrand: str
    paymentType: str
    result: TransactionResult
    resultDetails: dict
    connectorTxID1: str

    @classmethod
    def from_payment(cls, payment: Payment, transaction_status: ResultCode) -> "RedirectBack":
        """Instantiate the RedirectBack class using the payment object.

        Args:
            payment: (Payment) Payment object
            transaction_status: (ResultCode) Transaction status

        Returns:
            RedirectBack: RedirectBack object
        """
        return cls(
            uniqueId=payment.unique_id,
            amount=payment.amount,
            currency=payment.currency,
            paymentBrand=payment.payment_brand,
            paymentType=payment.payment_type,
            result=dict(code=transaction_status.code),
            resultDetails=dict(),
            connectorTxID1=str(payment.id),
        )
