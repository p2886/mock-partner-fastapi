"""Operations used by redirect endpoints."""

import logging
from datetime import datetime, timezone

from fastapi import HTTPException, status
from fastapi.responses import RedirectResponse
from peachpayments_partner.result_codes import result_codes
from peachpayments_partner_pydantic.schemas import WebhookRequest
from pydantic import UUID4
from sqlalchemy.orm import Session

from app.internal import actions as internal_actions
from app.internal import schemas as internal_schemas
from app.models import Payment
from app.webhooks.actions import create_webhook

logger = logging.getLogger(__name__)


def validate_redirect(db: Session, payment_id: UUID4) -> Payment:
    """Retrieves the Payment object and validates if it wasn't already processed.

    Args:
        db: (Session) SQLAlchemy session object.
        payment_id: (UUID4) Payment ID.

    Returns:
        Payment object.

    Raises:
        HTTPException: If the Payment wasn't found or was already processed.
    """
    payment = internal_actions.payment.get(db, payment_id)
    if not payment:
        logger.error(f"id={payment_id} Payment not found")
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Payment not found")

    if payment.status_code != result_codes.TRANSACTION_PENDING.code:
        logger.error(f"id={payment_id} Payment already processed")
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Payment already processed",
        )

    return payment


def handle_redirect_response(db: Session, payment_id: UUID4, should_pass: bool) -> RedirectResponse:
    """Updates Payment status_code and creates a new Status.

    Webhook is called after the status changes.
    Synchronous POST request is placed to store the transaction status.
    Browser is ordered to redirect to the shopper_result_url.

    Args:
        db: (Session) SQLAlchemy session object.
        payment_id: (UUID4) Payment ID.
        should_pass: (bool) Whether the payment should be passed or not.

    Returns:
        RedirectResponse object.

    Raises:
        AssertionError: If the update transaction status is not HTTP_204_NO_CONTENT.
    """
    logger.info(f"id={payment_id} Redirect user response received, should_pass={should_pass}")
    payment = validate_redirect(db, payment_id)
    # should_pass set by user in redirect part of debit flow
    if should_pass:
        transaction_status = result_codes.TRANSACTION_SUCCEEDED
    else:
        transaction_status = result_codes.TRANSACTION_DECLINED_FOR_UNKNOWN_REASON

    # Update Payment
    payment = internal_actions.payment.update(db, db_obj=payment, obj_in=dict(status_code=transaction_status.code))
    # Create final Status
    internal_actions.status.create(
        db,
        obj_in=internal_schemas.StatusCreate(
            payment_id=payment.id,
            status_code=transaction_status.code,
            status_description=transaction_status.description,
        ),
    )
    # send webhook notification
    webhook_payload_dict = {
        "uniqueId": payment.unique_id,
        "amount": payment.amount,
        "currency": payment.currency,
        "paymentBrand": payment.payment_brand,
        "paymentType": payment.payment_type,
        "result": {"code": payment.status_code},
        "connectorTxID1": str(payment.id),
        "timestamp": datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
    }
    logger.debug(
        f"id={payment_id} uid={payment.unique_id} shopper_result_url={payment.shopper_result_url} - Redirecting"
    )
    WebhookRequest(**webhook_payload_dict)
    create_webhook(db, webhook_payload_dict, payment, payment.notification_url)
    headers = {"Content-Type": "application/json"}
    return RedirectResponse(
        url=f"{payment.shopper_result_url}?uniqueId={payment.unique_id}",
        status_code=status.HTTP_302_FOUND,
        headers=headers,
    )
