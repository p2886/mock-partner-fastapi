"""View and modify internal models - Payments ans Statuses."""

from datetime import datetime
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

from fastapi.encoders import jsonable_encoder
from pydantic import UUID4, BaseModel
from sqlalchemy.orm import Query, Session
from sqlalchemy.sql.expression import and_, or_

from app.db import Base
from app.models import Payment, Status, Webhook

from . import schemas

# Define custom types for SQLAlchemy model, and Pydantic schemas
ModelType = TypeVar("ModelType", bound=Base)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class BaseActions(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
    """Base class that can be extend by other action classes.

    Provides basic CRUD and listing operations.
    """

    def __init__(self, model: Type[ModelType]):
        """Assign model to the class.

        Args:
            model: The SQLAlchemy model
        """
        self.model = model

    def get_all(self, db: Session, *, skip: int = 0, limit: int = 100) -> List[ModelType]:
        """Get all items of ModelType.

        Args:
            db: (Session) SQLAlchemy session
            skip: (int) Number of items to skip
            limit: (int) Number of items to return

        Returns:
            List of ModelType
        """
        # Query db for all records of model skipping a minimum of 0 and displaying maximum of 100
        return db.query(self.model).offset(skip).limit(limit).all()

    def get(self, db: Session, id: Union[str, UUID4]) -> Optional[ModelType]:
        """Get an item of ModelType by id.

        Args:
            db: (Session) SQLAlchemy session
            id: (UUID4) The id of the item

        Returns:
            The item of ModelType or None if not found.
        """
        # Query for single record using unique Id
        # Whats the difference between UUID and unique ID?
        return db.query(self.model).filter(self.model.id == id).first()

    def create(self, db: Session, *, obj_in: CreateSchemaType) -> ModelType:
        """Create item of ModelType in database.

        Args:
            db: (Session) SQLAlchemy session
            obj_in: (CreateSchemaType) The object to create

        Returns:
            The created item of ModelType
        """
        # Convert obj bytes to JSON compatible version
        obj_in_data = jsonable_encoder(obj_in)
        # Instantiate db_obj using model
        db_obj = self.model(**obj_in_data)  # type: ignore
        # Save to db and refresh db
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update(self, db: Session, *, db_obj: ModelType, obj_in: Union[UpdateSchemaType, Dict[str, Any]]) -> ModelType:
        """Update item of ModelType in database.

        Args:
            db: (Session) SQLAlchemy session
            db_obj: (ModelType) The item of ModelType to update
            obj_in: (UpdateSchemaType or dict) The data to update

        Returns:
            The updated item of ModelType
        """
        # Convert obj bytes to JSON compatible version
        obj_data = jsonable_encoder(db_obj)
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            # Convert pydantic model into dictionary that excludes any fields that are not set
            update_data = obj_in.dict(exclude_unset=True)
        # Update changes attributes
        for field in obj_data:
            if field in update_data:
                setattr(db_obj, field, update_data[field])

        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj


class PaymentActions(BaseActions[Payment, schemas.PaymentCreate, schemas.PaymentUpdate]):
    """Payment actions extending basic CRUD operations."""

    def get_by_unique_id(self, db: Session, unique_id: str) -> Optional[Payment]:
        """Get Payment item identified by unique_id provided by PeachPayments.

        Args:
            db: (Session) SQLAlchemy session
            unique_id: (str) The unique_id (from PeachPayments) of the item

        Returns:
            The Payment item or None if not found.
        """
        return db.query(self.model).filter(self.model.unique_id == unique_id).first()

    def update_by_unique_id(self, db: Session, unique_id: str, payment_type: str) -> Optional[Payment]:
        """Update payment type of Payment item identified by unique_id provided by PeachPayments.

        Args:
            db (Session): (Session) SQLAlchemy session
            unique_id (str): The unique_id (from PeachPayments) of the item
            payment_type (str): The new payment type, one of types defined PaymentType

        Returns:
            Optional[Payment]: The updated Payment item or None if not found.
        """
        existing_transaction = db.query(self.model).filter(self.model.unique_id == unique_id).first()
        if not existing_transaction:
            return None
        existing_transaction.payment_type = payment_type
        db.commit()
        return existing_transaction


class StatusActions(BaseActions[Status, schemas.StatusCreate, schemas.StatusUpdate]):
    """Status actions extending basic CRUD operations."""

    def get_by_payment_id(self, db: Session, payment_id: UUID4, skip: int = 0, limit: int = 100) -> List[Status]:
        """Retrieves and returns a list of all statuses related to a payment.

        Args:
            db: (Session) SQLAlchemy session
            payment_id: (UUID4) The id of the payment
            skip: (int) Number of items to skip
            limit: (int) Number of items to return

        Returns:
            List of Statuses
        """

        return db.query(self.model).filter_by(payment_id=payment_id).offset(skip).limit(limit).all()


class WebhookActions(BaseActions[Webhook, schemas.WebhookCreate, schemas.WebhookUpdate]):
    """Webhook actions with basic CRUD operations."""

    def get_by_unique_id(self, db: Session, unique_id: str) -> Optional[Webhook]:
        """Retrieve webhook information using unique id."""
        return db.query(self.model).filter(self.model.unique_id == unique_id).first()

    def filter_active(self, db: Session, id: Optional[Union[str, UUID4]] = None) -> Query:
        """Filter active webhooks.

        Limit to given id if provided.

        Args:
            db: (Session) SQLAlchemy session
            id: (Optional[Union[str, UUID4]]) The id of the webhook
        """
        timestamp = datetime.utcnow().timestamp()
        filter = db.query(self.model).filter(
            and_(
                ~self.model.is_delivered,
                ~self.model.is_failed,
                or_(self.model.trigger_timestamp <= timestamp, self.model.trigger_timestamp is None),
            )
        )
        if id is not None:
            return filter.filter_by(id=id)

        return filter

    def get_active(self, db: Session, id: Union[str, UUID4]) -> Optional[Webhook]:
        """Retrieve active webhook using id."""
        return self.filter_active(db, id=id).first()


payment = PaymentActions(Payment)
webhook = WebhookActions(Webhook)
status = StatusActions(Status)
