"""Definitions of the [pydantic models](https://pydantic-docs.helpmanual.io/usage/models/).

These models will be used to validate the incoming request body and more
"""

from datetime import datetime
from typing import Optional

from peachpayments_partner_pydantic.outbound_schemas import PaymentType
from pydantic import UUID4, AnyHttpUrl, BaseModel, root_validator


class PaymentBase(BaseModel):
    """Base model for Payment REST."""

    unique_id: str
    amount: str
    currency: str
    payment_brand: str
    payment_type: str
    merchant_name: Optional[str] = None
    merchant_transaction_id: Optional[str] = None
    merchant_invoice_id: Optional[str] = None
    notification_url: str
    shopper_result_url: Optional[str] = None
    status_code: str
    error_message: Optional[str] = None
    created_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None
    payment_unique_id: Optional[str] = None
    clearing_institute_session_id: Optional[str] = None
    custom_parameters: Optional[str] = None


# Properties to receive via API on creation
class RefundCreate(PaymentBase):
    """Create refund fields."""


# Properties to receive via API on creation
class PaymentCreate(PaymentBase):
    """Create Payment fields."""

    @root_validator
    def validate_payment_parameters(cls, values):
        """Validate the entire PaymentCreate model data."""

        payment_type = values.get("payment_type")
        shopper_result_url = values.get("shopper_result_url")
        payment_unique_id = values.get("payment_unique_id")
        if not shopper_result_url and payment_type == PaymentType.DB.value:
            raise ValueError("Missing required parameter shopper_result_url")
        if not payment_unique_id and payment_type == PaymentType.RF.value:
            raise ValueError("Missing required parameter payment_unique_id")
        return values


# Properties to receive via API on update
class PaymentUpdate(PaymentBase):
    """Update Payment fields."""

    pass


class PaymentInDBBase(PaymentBase):
    """Payment in database requires an id field."""

    id: Optional[UUID4] = None

    class Config:
        """Switch to ORM mode."""

        orm_mode = True


class StatusBase(BaseModel):
    """Base model for Status REST."""

    payment_id: UUID4
    status_code: str
    status_description: Optional[str] = None
    error_message: Optional[str] = None
    created_at: Optional[datetime] = None


class StatusCreate(StatusBase):
    """Create Status fields."""

    pass


class WebhookBase(BaseModel):
    # Payment.id
    payment_id: UUID4
    # Peach unique_id
    unique_id: str
    # Data contains a JSON object which will instantiate the WebhookRequest model
    data: str
    # has webhook been delivered
    is_delivered: Optional[bool] = False
    # has webhook been failed
    is_failed: Optional[bool] = False
    # URL which will be used to send the WebhookRequest
    notification_url: AnyHttpUrl
    # Current attempt number
    attempt: Optional[int] = 0
    # When should we send the WebhookRequest again
    trigger_timestamp: int
    created_at: Optional[datetime] = None


class WebhookCreate(WebhookBase):
    pass


class WebhookUpdate(WebhookBase):
    pass


class StatusUpdate(StatusBase):
    """Update Status fields."""

    pass


class StatusInDBBase(StatusBase):
    """Payment in database requires an id field."""

    id: Optional[UUID4] = None

    class Config:
        """Switch to ORM mode."""

        orm_mode = True


class WebhookInDB(WebhookBase):
    id: Optional[UUID4] = None

    class Config:
        orm_mode = True


# Additional properties to return via API
class Payment(PaymentInDBBase):
    """Payment definition for API."""

    pass


class Status(StatusInDBBase):
    """Status definition for API."""

    pass


class HTTPError(BaseModel):
    """HTTPError definition."""

    detail: str

    class Config:
        """Example for the documentation."""

        schema_extra = {
            "example": {"detail": "HTTPException raised."},
        }
