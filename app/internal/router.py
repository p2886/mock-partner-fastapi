"""Definition of the endpoints in the internal part of the app."""

from typing import List

from fastapi import APIRouter, Depends, HTTPException
from pydantic import UUID4
from sqlalchemy.orm import Session
from starlette.status import HTTP_404_NOT_FOUND

from app import models
from app.db.session import get_db
from app.internal import actions, schemas

router = APIRouter(tags=["internal"])


@router.get("/payments", response_model=List[schemas.Payment])
def list_payments(*, db: Session = Depends(get_db), skip: int = 0, limit: int = 100) -> List[models.Payment]:
    r"""GET list of payments.

    \f
    Args:
        db: (Session) SQLAlchemy session.
        skip: (int) Number of payments to skip.
        limit: (int) Number of payments to return.

    Returns:
        (List[Payment]) List of payments.
    """
    return actions.payment.get_all(db=db, skip=skip, limit=limit)


@router.get(
    "/payments/{payment_id}",
    response_model=schemas.Payment,
    responses={HTTP_404_NOT_FOUND: {"model": schemas.HTTPError}},
    tags=["internal"],
)
def get_payment(*, db: Session = Depends(get_db), payment_id: UUID4) -> models.Payment:
    r"""GET payment.

    \f
    Args:
        db: (Session) SQLAlchemy session.
        payment_id: (UUID4) Payment ID.

    Returns:
        (Payment) Payment.
    """
    payment = actions.payment.get(db=db, id=payment_id)
    if not payment:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Payment not found")

    return payment


@router.get("/statuses", response_model=List[schemas.Status])
def list_statuses(db: Session = Depends(get_db), skip: int = 0, limit: int = 100) -> List[models.Status]:
    r"""GET list of statuses.

    \f
    Args:
        db: (Session) SQLAlchemy session.
        skip: (int) Number of statuses to skip.
        limit: (int) Number of statuses to return.

    Returns:
        (List[Status]) List of statuses.
    """
    return actions.status.get_all(db=db, skip=skip, limit=limit)


@router.get("/payments/{payment_id}/statuses", response_model=List[schemas.Status])
def list_payment_statuses(
    payment_id: UUID4, db: Session = Depends(get_db), skip: int = 0, limit: int = 100
) -> List[models.Status]:
    r"""GET list of payment statuses.

    \f
    Args:
        payment_id: (UUID4) Payment ID.
        db: (Session) SQLAlchemy session.
        skip: (int) Number of statuses to skip.
        limit: (int) Number of statuses to return.

    Returns:
        (List[Status]) List of statuses.
    """
    return actions.status.get_by_payment_id(db=db, payment_id=payment_id, skip=skip, limit=limit)


@router.get(
    "/statuses/{status_id}",
    response_model=schemas.Status,
    responses={HTTP_404_NOT_FOUND: {"model": schemas.HTTPError}},
)
def get_status(*, db: Session = Depends(get_db), status_id: UUID4) -> models.Status:
    r"""GET status by id.

    \f
    Args:
        db: (Session) SQLAlchemy session.
        status_id: (UUID4) Status ID.

    Returns:
        (Status) Status.
    """
    status = actions.status.get(db=db, id=status_id)
    if not status:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Status not found")

    return status
