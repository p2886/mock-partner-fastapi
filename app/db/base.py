"""Create Base class for database models."""

from typing import Any

import inflect
from sqlalchemy.ext.declarative import as_declarative, declared_attr

p = inflect.engine()


@as_declarative()
class Base:
    """Base class inherited by all models."""

    id: Any
    __name__: str

    @declared_attr
    def __tablename__(self) -> str:
        """Generate __tablename__ automatically in plural form.

        i.e 'Payment' model will generate table name 'payments'
        """
        return p.plural(self.__name__.lower())
