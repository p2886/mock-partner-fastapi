# Import all the models, so that the Base class
# has them before being imported by Alembic.
from app import models  # noqa

from .base import Base  # noqa
