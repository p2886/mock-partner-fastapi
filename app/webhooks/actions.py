import json
import logging
from datetime import datetime
from typing import Union

import requests
from celery import Celery
from peachpayments_partner.authentication import get_access_token
from peachpayments_partner.exceptions import AuthError
from peachpayments_partner.jwt_formatter import TokenFormatter
from peachpayments_partner_pydantic.schemas import WebhookRequest, WebhookResponse
from pydantic import UUID4, AnyHttpUrl, ValidationError
from pydantic.tools import parse_obj_as
from sqlalchemy.orm import Session

from app.config import settings
from app.db.session import SessionLocal
from app.internal import actions as internal_actions
from app.internal import schemas as internal_schemas
from app.models import Payment, Webhook

celery = Celery("MockPartner", broker="amqp://rabbitmq:5672")
logger = logging.getLogger(__name__)

ATTEMPTS = {
    "1": 60,  # 1 minutes
    "2": 120,  # 2 minutes
    "3": 240,  # 4 minutes
    "4": 480,  # 8 minutes
    "5": 900,  # 15 minutes
    "6": 1800,  # 30 minutes
    "7": 3600,  # 60 minutes
    "8": 86400,  # 24 hours
}


def get_trigger_timestamp(attempt: Union[int, str]) -> int:
    """Collect the right trigger timestamp for the attempt.

    Args:
        attempt(int | str): attempt number

    Returns:
        timestamp(int): timestamp to trigger the webhook for the given attempt
    """
    current_trigger_timestamp = int(datetime.utcnow().timestamp())
    attempt_key = str(attempt)

    return current_trigger_timestamp + ATTEMPTS.get(attempt_key, 86400)


def create_webhook(
    db: Session, webhook_data: dict, payment: Payment, notification_url: Union[str, AnyHttpUrl]
) -> Webhook:
    """Create Webhook and schedule a celery task to send it.

    Args:
        db(Session): db object
        webhook_data(dict): webhook data to be parsed and stored in the database
        payment(Payment): payment object
        notification_url(AnyHttpUrl | str): notification url

    Returns:
        webhook(Webhook): created webhook object
    """
    if type(notification_url) == str:
        notification_url = parse_obj_as(AnyHttpUrl, notification_url)

    if payment.unique_id != webhook_data["uniqueId"]:
        logger.error(f"uid={payment.unique_id} - Debit transaction not found")
        raise ValueError("Payment unique id does not match webhook unique id")

    webhook_request = WebhookRequest(**webhook_data)
    webhook_in = internal_schemas.WebhookCreate(
        payment_id=payment.id,
        unique_id=payment.unique_id,
        notification_url=notification_url,
        trigger_timestamp=datetime.utcnow().timestamp(),
        data=webhook_request.json(exclude_none=True),
        is_delivered=False,
        is_failed=False,
    )
    webhook = internal_actions.webhook.create(
        db=db,
        obj_in=webhook_in,
    )
    logger.info(f"uid={webhook.unique_id} - Webhook created ({webhook.id})")
    send_webhook.delay(webhook.id)
    return webhook


def set_backoff(db: Session, webhook: Webhook) -> None:
    """Prepare webhook to be sent in the future.

    Updates the webhook object with the increased attempt number and sets the trigger timestamp.

    Args:
        db(Session): db object
        webhook(Webhook): webhook object
    """
    attempt = webhook.attempt + 1
    if attempt > settings.MAX_WEBHOOK_ATTEMPT:
        logger.warning(f"uid={webhook.unique_id} - Max webhook attempts reached")
        internal_actions.webhook.update(
            db,
            db_obj=webhook,
            obj_in=dict(
                is_failed=True,
            ),
        )
        return

    internal_actions.webhook.update(
        db,
        db_obj=webhook,
        obj_in=dict(
            attempt=attempt,
            trigger_timestamp=get_trigger_timestamp(attempt),
        ),
    )
    countdown = ATTEMPTS.get(str(attempt), 3600)
    logger.info(f"uid={webhook.unique_id} - Webhook will be sent in {countdown} seconds")
    send_webhook.apply_async(args=(webhook.id,), countdown=countdown)
    db.close()


@celery.task()
def send_webhook(webhook_id: Union[str, UUID4]) -> None:
    """Retrieve webhook from database and send it.

    Args:
        webhook_id(UUID4): webhook id of stored webhook
    """
    db = SessionLocal()
    # Some webhooks might be already sent in a former retry_webhooks call.
    webhook = internal_actions.webhook.get_active(db, webhook_id)
    if not webhook:
        db.close()
        return

    logger.info(f"uid={webhook.unique_id} - Sending webhook - authenticating inbound request.")
    try:
        authorization_token = get_access_token(
            client_id=settings.CLIENT_ID,
            client_secret=settings.CLIENT_SECRET,
            auth_service_generate_access_token_url=settings.AUTH_SERVICE_GENERATE_ACCESS_TOKEN_URL,
        )
    except AuthError:
        logger.exception(f"uid={webhook.unique_id} - Authentication error.")
        set_backoff(db, webhook)
        return

    headers = {"Authorization": TokenFormatter.format(authorization_token), "Content-Type": "application/json"}
    logger.info(f"uid={webhook.unique_id} - Sending webhook request to {webhook.notification_url}")
    try:
        response = requests.post(webhook.notification_url, json=json.loads(webhook.data), headers=headers, timeout=5)
    except requests.ConnectionError:
        logger.exception(f"uid={webhook.unique_id} - Webhook POST connection error.")
        set_backoff(db, webhook)
        return
    except Exception:
        logger.exception(f"uid={webhook.unique_id} - Unknown webhook POST error.")
        set_backoff(db, webhook)
        return

    if not 200 <= response.status_code < 300:
        logger.error(f"uid={webhook.unique_id} - Webhook POST error: {response.status_code}")
        set_backoff(db, webhook)
        return

    # validate Webhook response
    try:
        response_data = response.json()
    except json.JSONDecodeError:
        logger.error(f"uid={webhook.unique_id} - Webhook response JSON decode error, response: '{response.text}'")
        set_backoff(db, webhook)
        return

    try:
        WebhookResponse(**response_data)
    except ValidationError as exc_info:
        logger.error(f"uid={webhook.unique_id} - Webhook response validation error: {exc_info.errors()}")
        set_backoff(db, webhook)
        return

    internal_actions.webhook.update(
        db,
        db_obj=webhook,
        obj_in=dict(is_delivered=True),
    )
    logger.info(f"uid={webhook.unique_id} - Webhook delivered")
    db.close()
