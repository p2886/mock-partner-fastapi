"""Main application module."""

import logging
import logging.config
import random
import string
import time

from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse

from .peach.exception_handlers import validation_exception_handler
from .routes import router as api_router
from .utils.exception import APIException

logging.config.fileConfig("logging.conf")
logger = logging.getLogger(__name__)


def get_application() -> FastAPI:
    """Instantiates and returns FastAPI application.

    Required for testing.
    """
    application = FastAPI(
        exception_handlers={RequestValidationError: validation_exception_handler},
    )
    application.include_router(api_router)
    return application


app = get_application()
logger.info("application started")


@app.middleware("http")
async def log_requests(request: Request, call_next):
    idem = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
    logger.info(f"rid={idem} start request path={request.url.path}")
    start_time = time.perf_counter()

    response = await call_next(request)

    process_time = "{0:.4f}".format(time.perf_counter() - start_time)
    logger.info(f"rid={idem} completed_in={process_time}s status_code={response.status_code}")

    return response


@app.get("/")
def index():
    """Default endpoint."""
    return dict(message="[PeachPayments] Mock Partner App. See /docs for API documentation")


@app.exception_handler(APIException)
def exception_handler(request: Request, exc: APIException):
    """Return the handler if APIException raised."""
    logger.warning(f"APIException raised. status_code: {exc.status}, custom_message: {exc.custom_message}")
    return JSONResponse(status_code=exc.status, content=exc.custom_message)
