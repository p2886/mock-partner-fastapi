"""Definition of endpoints exposed to PeachPayments."""

from typing import Any

from fastapi import APIRouter, Depends
from fastapi import status as http_status
from peachpayments_partner_pydantic import schemas
from peachpayments_partner_pydantic.outbound_schemas import Error400Response, ErrorResponse
from sqlalchemy.orm import Session

from app.db.session import get_db
from app.peach import actions
from app.utils.auth import peach_auth_required

router = APIRouter(tags=["peach"])


@router.get(
    "/v1/status/{uniqueId}",
    response_model=schemas.StatusResponse,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"model": ErrorResponse},
        http_status.HTTP_400_BAD_REQUEST: {"model": Error400Response},
    },
    dependencies=[Depends(peach_auth_required)],
    response_model_exclude_none=True,
)
def transaction_status_request(*, db: Session = Depends(get_db), uniqueId: str) -> Any:
    """Entry point for status function.

    Args:
        db: db objects
        uniqueId: an id of the transaction in PeachPayments

    Returns:
        status response
    """
    return actions.status(db=db, unique_id=uniqueId)


@router.post(
    "/v1/debit",
    response_model=schemas.DebitResponse,
    responses={
        http_status.HTTP_400_BAD_REQUEST: {"model": Error400Response},
    },
    dependencies=[Depends(peach_auth_required)],
    response_model_exclude_none=True,
)
def debit_request(*, db: Session = Depends(get_db), debit_in: schemas.DebitRequest) -> Any:
    r"""Entry point for debit function.

    Args:
        db: db object
        debit_in: debit request schema object

    Returns:
        debit response
    """
    return actions.debit(db=db, data=debit_in)


@router.post(
    "/v1/refund/{debit_unique_id}",
    response_model=schemas.RefundResponse,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"model": ErrorResponse},
        http_status.HTTP_400_BAD_REQUEST: {"model": Error400Response},
    },
    dependencies=[Depends(peach_auth_required)],
    response_model_exclude_none=True,
)
def refund_request(
    *,
    db: Session = Depends(get_db),
    debit_unique_id: str,
    refund_data: schemas.RefundRequest,
) -> Any:
    r"""Entry point for refund function.

    Args:
        db: db object
        debit_unique_id: debit transaction unique Id to be refunded
        refund_data: refund request schema object

    Returns:
        refund response
    """
    return actions.refund(db=db, debit_unique_id=debit_unique_id, refund_data=refund_data)


@router.post(
    "/v1/cancel/{unique_id}",
    response_model=schemas.CancelResponse,
    responses={
        http_status.HTTP_404_NOT_FOUND: {"model": ErrorResponse},
        http_status.HTTP_400_BAD_REQUEST: {"model": Error400Response},
    },
    dependencies=[Depends(peach_auth_required)],
    response_model_exclude_none=True,
)
def cancel_request(
    *,
    db: Session = Depends(get_db),
    unique_id: str,
    cancel_data: schemas.CancelRequest,
) -> Any:
    r"""Entry point for cancel function.

    Args:
        db: db object
        unique_id: transaction unique Id to be cancelled
        cancel_data: cancel request schema object

    Returns:
        cancel response
    """
    return actions.cancel(
        db=db,
        unique_id=unique_id,
        cancel_data=cancel_data,
    )
