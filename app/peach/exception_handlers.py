"""Overriding ValidationError."""
from fastapi import Request, status
from fastapi.exception_handlers import request_validation_exception_handler
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from peachpayments_partner_pydantic.exception_handlers import exception_to_response

from . import router


async def validation_exception_handler(request: Request, exc: RequestValidationError) -> JSONResponse:
    """A custom validation exception for Peach endpoints.

    Checks if the endpoint was from peach.routes module and if so, returns a custom error.

    Args:
        request: The request object.
        exc: The exception object.

    Returns:
        A JSON response containing the ValidationErrorResult and the result code.
    """
    if (
        # Do not modify the validation response if endpoint not in peach router
        # Note: This is a workaround for the fact that fastapi doesn't support router scope
        #       exception_handlers.
        # Checks to see if endpoint's __name__ is in list of router's functions
        request.scope["endpoint"].__name__ not in dir(router)
        # Do not modify if field wasn't in the body
        or exc.body is None
    ):
        # Return original exception handler not special handler
        return await request_validation_exception_handler(request, exc)

    return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST, content=exception_to_response(exc))
