"""Operations used by endpoints required by PeachPayments."""

import json
import logging
import re
from datetime import datetime, timezone
from typing import Optional

from fastapi import status as http_status
from peachpayments_partner.error_response import format_error_response
from peachpayments_partner.result_codes import result_codes
from peachpayments_partner_pydantic.schemas import (
    CancelRequest,
    CancelResponse,
    DebitRequest,
    DebitResponse,
    RefundRequest,
    RefundResponse,
    StatusResponse,
)
from pydantic import AnyHttpUrl
from pydantic.tools import parse_obj_as
from sqlalchemy.orm import Session

from app.config import settings
from app.internal import actions as internal_actions
from app.internal import schemas as internal_schemas
from app.models import Payment
from app.utils.exception import APIException
from app.webhooks.actions import create_webhook

logger = logging.getLogger(__name__)


def status(db: Session, unique_id: str) -> StatusResponse:
    """Process status request.

    Args:
        db: db session object
        unique_id: unique ID string

    Returns:
        status response schema object

    Raises:
        APIException
    """

    logger.info(f"uid={unique_id} - Status request")
    transaction: Optional[Payment] = internal_actions.payment.get_by_unique_id(db=db, unique_id=unique_id)
    if not transaction:
        logger.error(f"uid={unique_id} - Transaction not found")
        response = format_error_response(result_codes.CANNOT_FIND_TRANSACTION)
        raise APIException(custom_message=response, status=http_status.HTTP_404_NOT_FOUND)

    return StatusResponse(
        uniqueId=transaction.unique_id,
        amount=transaction.amount,
        currency=transaction.currency,
        paymentBrand=transaction.payment_brand,
        paymentType=transaction.payment_type,
        result={"code": transaction.status_code},
        connectorTxID1=str(transaction.id),
        timestamp=datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        customParameters=(
            json.loads(transaction.custom_parameters) if transaction.custom_parameters is not None else None
        ),
    )


def debit(db: Session, data: DebitRequest) -> DebitResponse:
    """Start payment process.

    Args:
        db: db session object
        data: debit request schema object

    Returns:
        debit response schema object

    Raises:
        APIException
    """
    # TODO: Additional data verification
    # TODO: Check if a payment with given unique_id already exists

    # Store payment
    data_in = data.dict()
    logger.info(f"uid={data_in['uniqueId']} - Debit request")

    payment: Payment = internal_actions.payment.create(
        db=db,
        obj_in=internal_schemas.PaymentCreate(
            unique_id=data_in["uniqueId"],
            amount=data_in["amount"],
            currency=data_in["currency"],
            payment_brand=data_in["paymentBrand"],
            payment_type=data_in["paymentType"].value,
            merchant_name=data_in["merchantName"],
            merchant_transaction_id=data_in["merchantTransactionId"],
            merchant_invoice_id=data_in["merchantInvoiceId"],
            notification_url=data_in["notificationUrl"],
            shopper_result_url=data_in["shopperResultUrl"],
            clearing_institute_session_id=data_in["clearingInstituteSessionId"],
            custom_parameters=(
                json.dumps(data_in["customParameters"]) if data_in["customParameters"] is not None else None
            ),
            status_code=result_codes.TRANSACTION_PENDING.code,
        ),
    )

    # Store Status
    internal_actions.status.create(
        db=db,
        obj_in=internal_schemas.StatusCreate(
            payment_id=payment.id,
            status_code=payment.status_code,
            status_description=result_codes.get(payment.status_code).description,
        ),
    )

    # preparing the response to Peach
    debit_response_dict = {
        "uniqueId": payment.unique_id,
        "amount": payment.amount,
        "currency": payment.currency,
        "paymentBrand": payment.payment_brand,
        "paymentType": payment.payment_type,
        "result": {"code": payment.status_code},
        "redirect": {
            "url": settings.REDIRECT_URL,
            # Note:
            #     We're forcing POST redirect method for the debit transaction with amount equal "0.99"
            #     This is done for integral testing purposes.
            "method": "POST" if payment.amount == "0.99" else "GET",
            "parameters": [{"name": "payment_id", "value": str(payment.id)}],
        },
        "connectorTxID1": str(payment.id),
        "clearingInstituteSessionId": payment.clearing_institute_session_id,
        "customParameters": json.loads(payment.custom_parameters) if payment.custom_parameters is not None else None,
        "timestamp": datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
    }

    logger.info(f"uid={payment.unique_id} code={payment.status_code} - Sending debit response")
    debit_response = DebitResponse(**debit_response_dict)
    # send webhook notification for pending transaction
    create_webhook(db, debit_response_dict, payment, parse_obj_as(AnyHttpUrl, payment.notification_url))

    return debit_response


def refund(db: Session, debit_unique_id: str, refund_data: RefundRequest):
    """Start refund process.

    Args:
        db: db session object
        debit_unique_id: debit transaction unique ID string to be refunded
        refund_data: refund request schema object

    Returns:
        refund response schema object

    Raises:
        APIException
    """
    logger.info(f"debit_uid={debit_unique_id} - Refund request")

    success_status = "^(000.000.|000.100.1|000.[36])|^(000.400.0[^3]|000.400.100)"
    # use the original transaction id to check if original payment exists in database
    payment_transaction: Optional[Payment] = internal_actions.payment.get_by_unique_id(db=db, unique_id=debit_unique_id)
    # raise an error if transaction is not found using the transaction id
    if not payment_transaction:
        logger.error(f"debit_uid={debit_unique_id} - Debit transaction not found")
        response = format_error_response(result_codes.CANNOT_FIND_TRANSACTION)
        raise APIException(custom_message=response, status=http_status.HTTP_404_NOT_FOUND)

    refund_data_dict = refund_data.dict()
    logger.info(f"uid={refund_data_dict['uniqueId']} debit_uid={debit_unique_id} - Refund request")
    refund_transaction = internal_actions.payment.create(
        db=db,
        obj_in=internal_schemas.PaymentCreate(
            unique_id=refund_data_dict["uniqueId"],
            payment_unique_id=debit_unique_id,
            amount=refund_data_dict["amount"],
            currency=refund_data_dict["currency"],
            payment_brand=refund_data_dict["paymentBrand"],
            payment_type=refund_data_dict["paymentType"].value,
            notification_url=refund_data_dict["notificationUrl"],
            # storing custom_parameters as a string
            custom_parameters=(
                json.dumps(refund_data_dict["customParameters"])
                if refund_data_dict["customParameters"] is not None
                else None
            ),
            status_code=result_codes.SUCCESSFUL_REQUEST.code,
        ),
    )
    if not refund_transaction:
        logger.error(f"uid={refund_data_dict['uniqueId']} - Transaction not found")
        response = format_error_response(result_codes.CANNOT_FIND_TRANSACTION)
        raise APIException(custom_message=response, status=http_status.HTTP_404_NOT_FOUND)

    # check amount for transaction is correct or if status of the transaction is correct or if payment type is debit
    # these are not validation errors so a 400 status code response is not appropriate - we return a  200 response
    # with the CANNOT_REFUND_VOLUME_EXCEEDED result code.  We also create a payment db object for future reference.
    if (
        refund_data_dict["amount"] != payment_transaction.amount
        or not re.match(success_status, payment_transaction.status_code)
        or payment_transaction.payment_type != "DB"
    ):
        logger_message = f"uid={refund_data_dict['uniqueId']} - Cannot refund - "
        if refund_data_dict["amount"] != payment_transaction.amount:
            logger_message += (
                "refund amount not equal the original debit amount, "
                f"debit_amount={payment_transaction.amount} refund_amount={refund_data_dict['amount']}"
            )
        elif not re.match(success_status, payment_transaction.status_code):
            logger_message += (
                "original payment status is not successful, debit_status={payment_transaction_data.status_code}"
            )
        else:
            logger_message += "original payment type is not debit, debit_type={payment_transaction_data.payment_type}"

        logger.warning(logger_message)

        refund_transaction = internal_actions.payment.update(
            db=db,
            db_obj=refund_transaction,
            obj_in=internal_schemas.PaymentUpdate(
                unique_id=refund_transaction.unique_id,
                amount=refund_transaction.amount,
                currency=refund_transaction.currency,
                payment_brand=refund_transaction.payment_brand,
                payment_type=refund_transaction.payment_type,
                notification_url=refund_transaction.notification_url,
                status_code=result_codes.CANNOT_REFUND_REFUND_VOLUME_EXCEEDED_OR_TX_REVERSED_OR_INVALID_WORKFLOW.code,
            ),
        )

    # store refund status
    internal_actions.status.create(
        db=db,
        obj_in=internal_schemas.StatusCreate(
            payment_id=refund_transaction.id,
            status_code=refund_transaction.status_code,
            status_description=result_codes.get(payment_transaction.status_code).description,
        ),
    )

    # update response data
    refund_response_dict = {
        "uniqueId": refund_transaction.unique_id,
        "amount": refund_transaction.amount,
        "currency": refund_transaction.currency,
        "paymentBrand": refund_transaction.payment_brand,
        "paymentType": refund_transaction.payment_type,
        "result": {"code": refund_transaction.status_code},
        "connectorTxID1": str(refund_transaction.id),
        "customParameters": (
            json.loads(refund_transaction.custom_parameters)
            if refund_transaction.custom_parameters is not None
            else None
        ),
        "timestamp": datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
    }
    refund_response = RefundResponse(**refund_response_dict)
    logger.info(f"uid={refund_data_dict['uniqueId']} - Refund status: {refund_transaction.status_code}")
    # send webhook notification for refund complete
    create_webhook(
        db, refund_response_dict, refund_transaction, parse_obj_as(AnyHttpUrl, refund_transaction.notification_url)
    )
    return refund_response


def cancel(db: Session, unique_id: str, cancel_data: CancelRequest):
    """Start cancel process.

    Args:
        db: db session object
        unique_id: transaction unique ID string to be cancelled
        cancel_data: cancel request schema object

    Returns:
        cancel response schema object

    Raises:
        APIException
    """
    logger.info(f"uid={unique_id} - Cancel request")
    # use the original transaction id to check if original payment exists in database
    payment_transaction_data: Optional[Payment] = internal_actions.payment.get_by_unique_id(db=db, unique_id=unique_id)
    # raise an error if transaction is not found using the transaction id
    if not payment_transaction_data:
        logger.error(f"uid={unique_id} - Transaction not found")
        response = format_error_response(result_codes.CANNOT_FIND_TRANSACTION)
        raise APIException(custom_message=response, status=http_status.HTTP_404_NOT_FOUND)

    # update only pending transaction status_code
    cancel_result_code = result_codes.CANCELLED_BY_USER.code
    pending_code = result_codes.TRANSACTION_PENDING.code
    if payment_transaction_data.status_code != pending_code:
        logger.error(
            f"uid={unique_id} - Transaction status is not pending, status_code={payment_transaction_data.status_code}"
        )
        response = format_error_response(
            result_codes.INVALID_TRANSACTION_FLOW_THE_REQUESTED_FUNCTION_IS_NOT_APPLICABLE_FOR_THE_REFERENCED_TRANSACTION
        )
        raise APIException(custom_message=response, status=http_status.HTTP_400_BAD_REQUEST)

    # XXX: check paymentBrand

    data_in = cancel_data.dict()

    # update transaction status
    cancel_transaction: Payment = internal_actions.payment.update(
        db=db,
        obj_in=internal_schemas.PaymentUpdate(
            unique_id=unique_id,
            amount=payment_transaction_data.amount,
            currency=payment_transaction_data.currency,
            payment_brand=payment_transaction_data.payment_brand,
            payment_type=payment_transaction_data.payment_type,
            notification_url=data_in["notificationUrl"],
            custom_parameters=json.dumps(data_in["customParameters"]),
            status_code=cancel_result_code,
        ),
        db_obj=payment_transaction_data,
    )

    # store cancel status
    internal_actions.status.create(
        db=db,
        obj_in=internal_schemas.StatusCreate(
            payment_id=cancel_transaction.id,
            status_code=cancel_transaction.status_code,
            status_description=result_codes.get(cancel_result_code).description,
        ),
    )

    logger.debug(f"uid={unique_id} - Cancel transaction created, cancel_transaction_id={cancel_transaction.id}")

    cancel_response_data = {
        "uniqueId": cancel_transaction.unique_id,
        "paymentBrand": cancel_transaction.payment_brand,
        "result": {"code": cancel_result_code},
        "connectorTxID1": str(cancel_transaction.id),
        "timestamp": datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        "customParameters": data_in["customParameters"],
    }

    cancel_response = CancelResponse(**cancel_response_data)
    cancel_webhook_request_data = cancel_response_data | {
        "amount": cancel_transaction.amount,
        "currency": cancel_transaction.currency,
        "paymentType": cancel_transaction.payment_type,
    }

    logger.info(f"uid={unique_id} - Transaction cancelled")

    # send webhook notification for cancellation complete
    create_webhook(
        db,
        cancel_webhook_request_data,
        cancel_transaction,
        parse_obj_as(AnyHttpUrl, cancel_transaction.notification_url),
    )
    return cancel_response
